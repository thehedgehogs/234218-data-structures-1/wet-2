#!/usr/bin/env bash

set -Eeuxo pipefail

NAME=submission_test.zip
FINAL=wet2.zip

# cmake --build build --target dry-pdf

zip -j "${NAME}" src/*.{hpp,cpp} submissions.txt HW2_-_wet_second_appeal.pdf dry/DS_DRY_3_2.pdf # build/Dry.pdf 

rm -i "${NAME}"
if [ -f "${NAME}" ]; then
    echo "Saving ${NAME} as ${FINAL}"
    mv "${NAME}" "${FINAL}"
fi
