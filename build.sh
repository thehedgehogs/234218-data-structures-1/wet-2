#!/usr/bin/env bash

set -Eeuxo pipefail

cmake -S . -B build "$@"
cmake --build build
