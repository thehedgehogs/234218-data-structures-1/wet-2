#!/usr/bin/env bash

set -Eeuxo pipefail

cmake -S . -B build "$@"
cmake --build build
pushd build
    ctest --output-on-failure
popd
