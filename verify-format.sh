#!/usr/bin/env bash

set -Eeuxo pipefail

if [[ $# -gt 0 ]]; then
    clang-format --dry-run -Werror $@
else
    clang-format --dry-run -Werror src/*.{c,h}pp {public,private}-tests/*.cc
fi
