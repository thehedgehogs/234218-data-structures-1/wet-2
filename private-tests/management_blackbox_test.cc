#include "management.hpp"

#include <gsl/gsl>
#include <gtest/gtest.h>

namespace ds2 {
namespace testing {

    static constexpr const double PRECISION{0.001};

    TEST(ExamplesFromInstructions, page_5_sum_of_bump_grade) {
        Management management{1};

        ASSERT_EQ(SUCCESS, management.add_employee(4, 1, 1));
        ASSERT_EQ(SUCCESS, management.increase_employee_salary(4, 2));
        ASSERT_EQ(SUCCESS, management.add_employee(5, 1, 4));
        ASSERT_EQ(SUCCESS, management.increase_employee_salary(5, 4));
        ASSERT_EQ(SUCCESS, management.add_employee(6, 1, 4));
        ASSERT_EQ(SUCCESS, management.increase_employee_salary(6, 2));

        const unsigned long not_set{0};
        unsigned long company1_result{not_set};
        unsigned long global_result{not_set};
        const unsigned long expected{8};

        ASSERT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(1, 2, &company1_result));
        EXPECT_EQ(expected, company1_result);

        ASSERT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(0, 2, &global_result));
        EXPECT_EQ(expected, company1_result);
    }

    TEST(ExamplesFromInstructions, page_6_average_bump_grade) {
        Management management{1};

        ASSERT_EQ(SUCCESS, management.add_employee(1, 1, 1));
        ASSERT_EQ(SUCCESS, management.increase_employee_salary(1, 2));
        ASSERT_EQ(SUCCESS, management.add_employee(2, 1, 4));
        ASSERT_EQ(SUCCESS, management.increase_employee_salary(2, 10));

        const double not_set{0};
        double company1_result1{not_set};
        double global_result1{not_set};
        const double expected1{2.5L};

        ASSERT_EQ(SUCCESS, management.average_bump_grade_between_salaries_by_company(1, 0, 10, &company1_result1));
        EXPECT_NEAR(expected1, company1_result1, PRECISION);

        ASSERT_EQ(SUCCESS, management.average_bump_grade_between_salaries_by_company(0, 0, 10, &global_result1));
        EXPECT_NEAR(expected1, company1_result1, PRECISION);

        double company1_result2{not_set};
        double global_result2{not_set};
        const double expected2{4L};

        ASSERT_EQ(SUCCESS, management.average_bump_grade_between_salaries_by_company(1, 8, 12, &company1_result2));
        EXPECT_NEAR(expected2, company1_result2, PRECISION);

        ASSERT_EQ(SUCCESS, management.average_bump_grade_between_salaries_by_company(0, 8, 12, &global_result2));
        EXPECT_NEAR(expected2, company1_result2, PRECISION);
    }

    TEST(Management_sum_bump_Test, check_validity_of_m) {
        Management management{2};
        unsigned long result{0};
        EXPECT_EQ(management.add_employee(1, 1, 1), SUCCESS);
        EXPECT_EQ(management.increase_employee_salary(1, 1), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 0, &result), INVALID_INPUT);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 0, &result), INVALID_INPUT);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 1, &result), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 1, &result), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 2, &result), FAILURE);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 2, &result), FAILURE);

        EXPECT_EQ(management.add_employee(2, 1, 1), SUCCESS);
        EXPECT_EQ(management.add_employee(3, 2, 1), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 1, &result), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 1, &result), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 2, &result), FAILURE);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 2, &result), FAILURE);

        EXPECT_EQ(management.increase_employee_salary(2, 1), SUCCESS);
        EXPECT_EQ(management.increase_employee_salary(3, 1), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 2, &result), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 2, &result), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 3, &result), FAILURE);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 3, &result), SUCCESS);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 4, &result), FAILURE);
    }

    TEST(Management_sum_bump_Test, one_company_no_workers) {
        Management management{1};
        unsigned long result{0};

        EXPECT_EQ(FAILURE, management.sum_of_bump_grade_between_top_workers_by_company(1, 1, &result));
        EXPECT_EQ(0, result);
        EXPECT_EQ(FAILURE, management.sum_of_bump_grade_between_top_workers_by_company(1, 9, &result));
        EXPECT_EQ(0, result);
        EXPECT_EQ(FAILURE, management.sum_of_bump_grade_between_top_workers_by_company(0, 99, &result));
        EXPECT_EQ(0, result);

        for (int i{1}; i < 10; ++i) {
            management.add_employee(i, 1, i);
            EXPECT_EQ(FAILURE, management.sum_of_bump_grade_between_top_workers_by_company(1, 1, &result));
            EXPECT_EQ(0, result);
        }

        EXPECT_EQ(FAILURE, management.sum_of_bump_grade_between_top_workers_by_company(1, 1, &result));
        EXPECT_EQ(FAILURE, management.sum_of_bump_grade_between_top_workers_by_company(0, 1, &result));
        EXPECT_EQ(0, result);
    }

    TEST(Management_sum_bump_Test, many_companies_no_workers) {
        GTEST_SKIP();
        Management management{10};
        unsigned long result{0};

        for (int i{1}; i < 100; ++i) {
            management.add_employee(i, (i % 10) + 1, i);
            EXPECT_EQ(FAILURE,
                      management.sum_of_bump_grade_between_top_workers_by_company((i % 10) + 1, (i % 10) + 1, &result));
            EXPECT_EQ(FAILURE, management.sum_of_bump_grade_between_top_workers_by_company(0, (i % 10) + 1, &result));
            EXPECT_EQ(0, result);
        }

        EXPECT_EQ(FAILURE, management.sum_of_bump_grade_between_top_workers_by_company(1, 1, &result));
        EXPECT_EQ(0, result);
    }

    TEST(Management_sum_bump_Test, one_company_one_worker) {
        Management management{1};
        unsigned long result{0};
        EXPECT_EQ(management.add_employee(1, 1, 1), SUCCESS);
        EXPECT_EQ(management.increase_employee_salary(1, 1), SUCCESS);

        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 1, &result), SUCCESS);
        EXPECT_EQ(result, 1);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 1, &result), SUCCESS);
        EXPECT_EQ(result, 1);

        EXPECT_EQ(management.promote_employee(1, 1), SUCCESS);
        EXPECT_EQ(management.promote_employee(1, 2), SUCCESS);

        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(1, 1, &result), SUCCESS);
        EXPECT_EQ(result, 4);
        EXPECT_EQ(management.sum_of_bump_grade_between_top_workers_by_company(0, 1, &result), SUCCESS);
        EXPECT_EQ(result, 4);
    }

    TEST(Management_sum_bump_Test, one_company_many_workers_same_salary) {
        GTEST_SKIP();
        Management management{1};
        unsigned long result{0};
        int company{1};
        int bump_grade{1};

        int num_of_employees{100};
        auto calc_grade = [&num_of_employees](int i) { return num_of_employees - i; };
        for (int i{1}; i < num_of_employees; ++i) {
            int employee{i * 2};
            EXPECT_EQ(SUCCESS, management.add_employee(employee, company, calc_grade(i)));
            EXPECT_EQ(SUCCESS, management.promote_employee(employee, bump_grade));

            unsigned long temp_result{0};
            for (int j{1}; j <= i; ++j) {
                temp_result += static_cast<unsigned long>(calc_grade(j));
            }

            EXPECT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(company, i, &result));
            EXPECT_EQ(result, temp_result);

            EXPECT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(0, i, &result));
            EXPECT_EQ(result, temp_result);
        }

        EXPECT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(company, 1, &result));
        EXPECT_EQ(result, num_of_employees - 1);

        EXPECT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(0, 1, &result));
        EXPECT_EQ(result, num_of_employees - 1);
    }

    TEST(Management_sum_bump_Test, one_company_many_workers_different_salaries) {
        Management management{1};
        unsigned long result{0};
        int company{1};

        int num_of_employees{20};
        auto calc_grade = [&num_of_employees](int i) { return num_of_employees + i; };
        for (int i{1}; i < num_of_employees; ++i) {
            int employee{i * 2};
            int salary{employee * 10};
            EXPECT_EQ(SUCCESS, management.add_employee(employee, company, calc_grade(i)));
            EXPECT_EQ(SUCCESS, management.increase_employee_salary(employee, salary));

            unsigned long temp_result{0};
            for (int j{i}; j >= 1; --j) {
                temp_result += static_cast<unsigned long>(calc_grade(j));
            }

            EXPECT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(company, i, &result));
            EXPECT_EQ(result, temp_result);

            EXPECT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(0, i, &result));
            EXPECT_EQ(result, temp_result);
        }
    }

    TEST(Management_sum_bump_Test, many_companies_many_workers) {
        GTEST_SKIP();
        const int num_of_companies{10};
        Management management{num_of_companies};
        unsigned long result{0};

        const int num_of_employees{100};  // make sure it divides num_of_companies!
        auto calc_grade = [](int i) { return num_of_employees * i; };
        auto calc_company = [](int i) {
            return static_cast<std::size_t>(((i - 1) % num_of_companies)) + 1;
        };  // 1 to num_of_companies, over and over
        for (int i{1}; i <= num_of_employees; ++i) {
            int company{static_cast<int>(calc_company(i))};
            int employee{i * 3};
            int salary{employee / 3};
            EXPECT_EQ(SUCCESS, management.add_employee(employee, company, calc_grade(i)));
            EXPECT_EQ(SUCCESS, management.increase_employee_salary(employee, salary));

            std::unique_ptr<unsigned long[]> temp_result_company{new unsigned long[num_of_companies]};
            for (std::size_t j{0}; j < num_of_companies; ++j) {
                temp_result_company[j] = 0;
            }
            unsigned long temp_result_total{0};

            for (int j{1}; j <= i; ++j) {
                unsigned long grade{static_cast<unsigned long>(calc_grade(j))};
                temp_result_total += grade;
                temp_result_company[calc_company(j)] += grade;
            }

            for (int j{1}; j < num_of_companies; ++j) {
                EXPECT_EQ(SUCCESS,
                          management.sum_of_bump_grade_between_top_workers_by_company(
                              company, num_of_employees / num_of_companies, &result));
                EXPECT_EQ(result, temp_result_company[calc_company(j)]);

                EXPECT_EQ(SUCCESS, management.sum_of_bump_grade_between_top_workers_by_company(0, i, &result));
                EXPECT_EQ(result, temp_result_total);
            }
        }
    }

    TEST(OfficialTests, test2) {
        gsl::owner<void *> management{Init(10)};
        ASSERT_NE(nullptr, management);

        ASSERT_EQ(SUCCESS, AddEmployee(management, 17, 7, 29));
        ASSERT_EQ(SUCCESS, AddEmployee(management, 9, 3, 10));
        ASSERT_EQ(INVALID_INPUT, AddEmployee(management, 25, 56, -2));
        ASSERT_EQ(SUCCESS, AddEmployee(management, 13, 8, 14));
        ASSERT_EQ(SUCCESS, AddEmployee(management, 4, 8, 7));
        ASSERT_EQ(SUCCESS, AddEmployee(management, 12, 6, 10));
        ASSERT_EQ(SUCCESS, RemoveEmployee(management, 12));
        ASSERT_EQ(FAILURE, RemoveEmployee(management, 7));
        ASSERT_EQ(SUCCESS, AddEmployee(management, 30, 2, 12));
        ASSERT_EQ(INVALID_INPUT, AcquireCompany(management, 2, 2, 10));
        ASSERT_EQ(INVALID_INPUT, EmployeeSalaryIncrease(management, 0, 32));
        ASSERT_EQ(SUCCESS, EmployeeSalaryIncrease(management, 17, 10));
        ASSERT_EQ(SUCCESS, EmployeeSalaryIncrease(management, 9, 1));
        ASSERT_EQ(INVALID_INPUT, PromoteEmployee(management, -1, 82));
        ASSERT_EQ(SUCCESS, PromoteEmployee(management, 9, 3));
        ASSERT_EQ(FAILURE, PromoteEmployee(management, 3, 9));
        ASSERT_EQ(SUCCESS, PromoteEmployee(management, 30, 4));

        ASSERT_EQ(SUCCESS, SumOfBumpGradeBetweenTopWorkersByGroup(management, 3, 1));
        unsigned long result1{0};
        ASSERT_EQ(
            SUCCESS,
            static_cast<Management *>(management)->sum_of_bump_grade_between_top_workers_by_company(3, 1, &result1));
        ASSERT_EQ(13, result1);

        ASSERT_EQ(SUCCESS, AverageBumpGradeBetweenSalaryByGroup(management, 0, 0, 10));
        double result2{-1};
        ASSERT_EQ(
            SUCCESS,
            static_cast<Management *>(management)->average_bump_grade_between_salaries_by_company(0, 0, 10, &result2));
        ASSERT_NEAR(15.8, result2, 0.001);

        ASSERT_EQ(SUCCESS, AcquireCompany(management, 3, 10, 25));

        ASSERT_EQ(SUCCESS, SumOfBumpGradeBetweenTopWorkersByGroup(management, 0, 1));
        unsigned long result3{0};
        ASSERT_EQ(
            SUCCESS,
            static_cast<Management *>(management)->sum_of_bump_grade_between_top_workers_by_company(0, 1, &result3));
        ASSERT_EQ(29, result3);

        ASSERT_EQ(SUCCESS, AverageBumpGradeBetweenSalaryByGroup(management, 7, 0, 50));
        double result4{-1};
        ASSERT_EQ(
            SUCCESS,
            static_cast<Management *>(management)->average_bump_grade_between_salaries_by_company(7, 0, 50, &result4));
        ASSERT_NEAR(29, result4, 0.001);

        ASSERT_EQ(INVALID_INPUT, CompanyValue(management, 16));

        Quit(&management);
        ASSERT_EQ(nullptr, management);
    }

}  // namespace testing
}  // namespace ds2
