#define private public
#include "management.hpp"
#undef private

#include <gsl/gsl>
#include <gtest/gtest.h>

namespace ds2 {
namespace testing {

    static constexpr const double PRECISION{0.001};

    TEST(ExamplesFromInstructions, page_2_acquisitions) {
        Management management{4};

        management.acquisitions.sets[1].result = 2;
        management.acquisitions.sets[2].result = 1;
        management.acquisitions.sets[3].result = 4;
        management.acquisitions.sets[4].result = 4;

        EXPECT_EQ(management.acquire_company(4, 3, 0.75), SUCCESS);
        const double not_set{-1};
        double company1_value1{not_set};
        double company2_value1{not_set};
        double company3_value1{not_set};
        double company4_value1{not_set};

        ASSERT_EQ(management.company_value(1, &company1_value1), SUCCESS);
        EXPECT_NEAR(company1_value1, 2, PRECISION);
        ASSERT_EQ(management.company_value(2, &company2_value1), SUCCESS);
        EXPECT_NEAR(company2_value1, 1, PRECISION);
        ASSERT_EQ(management.company_value(3, &company3_value1), SUCCESS);
        EXPECT_NEAR(company3_value1, 4, PRECISION);
        ASSERT_EQ(management.company_value(4, &company4_value1), SUCCESS);
        EXPECT_NEAR(company4_value1, 7, PRECISION);

        EXPECT_EQ(management.acquire_company(1, 4, 0.2), SUCCESS);
        double company1_value2{not_set};
        double company2_value2{not_set};
        double company3_value2{not_set};
        double company4_value2{not_set};

        ASSERT_EQ(management.company_value(1, &company1_value2), SUCCESS);
        EXPECT_NEAR(company1_value2, 3.4, PRECISION);
        ASSERT_EQ(management.company_value(2, &company2_value2), SUCCESS);
        EXPECT_NEAR(company2_value2, 1, PRECISION);
        ASSERT_EQ(management.company_value(3, &company3_value2), SUCCESS);
        EXPECT_NEAR(company3_value2, 4, PRECISION);
        ASSERT_EQ(management.company_value(4, &company4_value2), SUCCESS);
        EXPECT_NEAR(company4_value2, 7, PRECISION);

        EXPECT_EQ(management.acquire_company(1, 2, 1), SUCCESS);

        double company1_value3{not_set};
        double company2_value3{not_set};
        double company3_value3{not_set};
        double company4_value3{not_set};

        ASSERT_EQ(management.company_value(1, &company1_value3), SUCCESS);
        EXPECT_NEAR(company1_value3, 4.4, PRECISION);
        ASSERT_EQ(management.company_value(2, &company2_value3), SUCCESS);
        EXPECT_NEAR(company2_value3, 1, PRECISION);
        ASSERT_EQ(management.company_value(3, &company3_value3), SUCCESS);
        EXPECT_NEAR(company3_value3, 5, PRECISION);
        ASSERT_EQ(management.company_value(4, &company4_value3), SUCCESS);
        EXPECT_NEAR(company4_value3, 8, PRECISION);
    }

}  // namespace testing
}  // namespace ds2
