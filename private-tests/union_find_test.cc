#include "union_find.hpp"

#include <gtest/gtest.h>

namespace ds2 {
namespace testing {
    template<typename T>
    struct Extract {
        public:
            struct Result {
                    T sum;
            };

            static void unite_merge(Result &to, const Result &from) {
                to.sum += from.sum;
            }

            static void find_merge(Result &to, const Result &from) {
                to.sum += from.sum;
            }

            static void find_unmerge(Result &to, const Result &from) {
                to.sum -= from.sum;
            }

            static Result empty() {
                return {0};
            }

            static Result initialize(const T &input) {
                return {input};
            }
    };

    using UnionFind = ds2::UnionFind<Extract>;

    TEST(UnionFind, construct_and_destruct) {
        UnionFind union_find{30};
    }

    TEST(UnionFind, size) {
        UnionFind union_find1{1};
        UnionFind union_find2{10};
        UnionFind union_find3{11};
        UnionFind union_find4{100};

        EXPECT_EQ(union_find1.size(), 1);
        EXPECT_EQ(union_find2.size(), 10);
        EXPECT_EQ(union_find3.size(), 11);
        EXPECT_EQ(union_find4.size(), 100);
    }

    TEST(UnionFind, find) {
        UnionFind union_find{4};
        EXPECT_EQ(union_find.size(), 4);

        UnionFind::value_type set1 = union_find.find(1).second;
        UnionFind::value_type set2 = union_find.find(2).second;
        UnionFind::value_type set3 = union_find.find(3).second;
        UnionFind::value_type set4 = union_find.find(4).second;

        EXPECT_NE(set1, set2);
        EXPECT_NE(set1, set3);
        EXPECT_NE(set1, set4);
        EXPECT_NE(set2, set3);
        EXPECT_NE(set2, set4);
        EXPECT_NE(set3, set4);

        EXPECT_EQ(set1, union_find.find(1).second);
        EXPECT_EQ(set2, union_find.find(2).second);
        EXPECT_EQ(set3, union_find.find(3).second);
        EXPECT_EQ(set4, union_find.find(4).second);

        EXPECT_NE(set1, union_find.find(2).second);
        EXPECT_NE(set1, union_find.find(3).second);
        EXPECT_NE(set1, union_find.find(4).second);
        EXPECT_NE(set2, union_find.find(1).second);
        EXPECT_NE(set2, union_find.find(3).second);
        EXPECT_NE(set2, union_find.find(4).second);
        EXPECT_NE(set3, union_find.find(1).second);
        EXPECT_NE(set3, union_find.find(2).second);
        EXPECT_NE(set3, union_find.find(4).second);
        EXPECT_NE(set4, union_find.find(1).second);
        EXPECT_NE(set4, union_find.find(2).second);
        EXPECT_NE(set4, union_find.find(3).second);
    }

    TEST(UnionFind, unite) {
        UnionFind union_find{5};
        EXPECT_EQ(union_find.size(), 5);

        UnionFind::value_type set1 = union_find.find(1).second;
        UnionFind::value_type set2 = union_find.find(2).second;
        UnionFind::value_type set3 = union_find.find(3).second;
        UnionFind::value_type set4 = union_find.find(4).second;
        UnionFind::value_type set5 = union_find.find(5).second;

        UnionFind::value_type set12 = union_find.unite(set1, set2);
        EXPECT_EQ(union_find.size(), 4);
        UnionFind::value_type set34 = union_find.unite(set3, set4);
        EXPECT_EQ(union_find.size(), 3);

        set1 = union_find.find(1).second;
        set2 = union_find.find(2).second;
        set3 = union_find.find(3).second;
        set4 = union_find.find(4).second;
        set5 = union_find.find(5).second;

        EXPECT_EQ(union_find.find(1).second, union_find.find(2).second);
        EXPECT_NE(union_find.find(2).second, union_find.find(3).second);
        EXPECT_EQ(union_find.find(3).second, union_find.find(4).second);
        EXPECT_NE(union_find.find(4).second, union_find.find(5).second);
        EXPECT_EQ(set1, set2);
        EXPECT_NE(set2, set3);
        EXPECT_EQ(set3, set4);
        EXPECT_NE(set4, set5);
        EXPECT_EQ(set1, set12);
        EXPECT_EQ(set2, set12);
        EXPECT_EQ(set34, set3);
        EXPECT_EQ(set34, set4);
        EXPECT_NE(set12, set34);

        // nothing should change here
        UnionFind::value_type set112 = union_find.unite(set1, set12);
        set1 = union_find.find(1).second;
        set2 = union_find.find(2).second;
        set3 = union_find.find(3).second;
        set4 = union_find.find(4).second;
        set5 = union_find.find(5).second;
        set12 = union_find.find(1).second;
        set34 = union_find.find(3).second;
        EXPECT_EQ(union_find.size(), 3);
        EXPECT_EQ(union_find.find(1).second, union_find.find(2).second);
        EXPECT_EQ(set1, set2);
        EXPECT_EQ(set1, set12);
        EXPECT_EQ(set1, set112);
        EXPECT_EQ(set112, set12);

        UnionFind::value_type set125 = union_find.unite(set1, set5);
        set1 = union_find.find(1).second;
        set2 = union_find.find(2).second;
        set3 = union_find.find(3).second;
        set4 = union_find.find(4).second;
        set5 = union_find.find(5).second;
        set12 = union_find.find(1).second;
        set34 = union_find.find(3).second;
        set112 = union_find.find(1).second;
        EXPECT_EQ(union_find.size(), 2);
        EXPECT_EQ(union_find.find(1).second, union_find.find(2).second);
        EXPECT_EQ(union_find.find(1).second, union_find.find(5).second);
        EXPECT_EQ(union_find.find(2).second, union_find.find(5).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(1).second);
        EXPECT_NE(union_find.find(1).second, union_find.find(3).second);
        EXPECT_NE(union_find.find(5).second, union_find.find(3).second);
        EXPECT_NE(union_find.find(5).second, union_find.find(4).second);
        EXPECT_NE(union_find.find(4).second, union_find.find(2).second);
        EXPECT_EQ(set1, set2);
        EXPECT_EQ(set1, set5);
        EXPECT_EQ(set2, set5);
        EXPECT_EQ(set5, set1);
        EXPECT_NE(set1, set3);
        EXPECT_NE(set5, set3);
        EXPECT_NE(set5, set4);
        EXPECT_NE(set4, set2);
        EXPECT_EQ(set125, set12);
        EXPECT_EQ(set125, set112);
        EXPECT_EQ(set125, set1);
        EXPECT_EQ(set125, set5);


        UnionFind::value_type set12345 = union_find.unite(set125, set3);
        set1 = union_find.find(1).second;
        set2 = union_find.find(2).second;
        set3 = union_find.find(3).second;
        set4 = union_find.find(4).second;
        set5 = union_find.find(5).second;
        set12 = union_find.find(1).second;
        set34 = union_find.find(3).second;
        set112 = union_find.find(1).second;
        set125 = union_find.find(1).second;
        EXPECT_EQ(union_find.size(), 1);
        EXPECT_EQ(union_find.find(1).second, union_find.find(2).second);
        EXPECT_EQ(union_find.find(1).second, union_find.find(3).second);
        EXPECT_EQ(union_find.find(1).second, union_find.find(4).second);
        EXPECT_EQ(union_find.find(1).second, union_find.find(5).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(2).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(3).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(4).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(5).second);
        EXPECT_EQ(set1, set2);
        EXPECT_EQ(set1, set3);
        EXPECT_EQ(set1, set4);
        EXPECT_EQ(set1, set5);
        EXPECT_EQ(set5, set2);
        EXPECT_EQ(set5, set3);
        EXPECT_EQ(set5, set4);
        EXPECT_EQ(set5, set5);
        EXPECT_EQ(set12, set5);
        EXPECT_EQ(set34, set5);
        EXPECT_EQ(set112, set5);
        EXPECT_EQ(set12345, set5);
        EXPECT_EQ(set12345, set34);
        EXPECT_EQ(set12345, set12345);

        // nothing should change here
        UnionFind::value_type set12345again = union_find.unite(set125, set3);
        set1 = union_find.find(1).second;
        set2 = union_find.find(2).second;
        set3 = union_find.find(3).second;
        set4 = union_find.find(4).second;
        set5 = union_find.find(5).second;
        set12 = union_find.find(1).second;
        set34 = union_find.find(3).second;
        set112 = union_find.find(1).second;
        set125 = union_find.find(1).second;
        set12345 = union_find.find(1).second;
        EXPECT_EQ(union_find.size(), 1);
        EXPECT_EQ(union_find.find(1).second, union_find.find(2).second);
        EXPECT_EQ(union_find.find(1).second, union_find.find(3).second);
        EXPECT_EQ(union_find.find(1).second, union_find.find(4).second);
        EXPECT_EQ(union_find.find(1).second, union_find.find(5).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(2).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(3).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(4).second);
        EXPECT_EQ(union_find.find(5).second, union_find.find(5).second);
        EXPECT_EQ(set1, set2);
        EXPECT_EQ(set1, set3);
        EXPECT_EQ(set1, set4);
        EXPECT_EQ(set1, set5);
        EXPECT_EQ(set5, set2);
        EXPECT_EQ(set5, set3);
        EXPECT_EQ(set5, set4);
        EXPECT_EQ(set5, set5);
        EXPECT_EQ(set12, set5);
        EXPECT_EQ(set34, set5);
        EXPECT_EQ(set112, set5);
        EXPECT_EQ(set12345, set5);
        EXPECT_EQ(set12345, set34);
        EXPECT_EQ(set12345, set12345again);
    }

}  // namespace testing
}  // namespace ds2
