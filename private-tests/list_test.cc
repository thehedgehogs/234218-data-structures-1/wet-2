#include "linked_list.hpp"

#include <gtest/gtest.h>
#include <string>

class A {};

TEST(LinkedList, construct_and_destruct) {
    ds2::LinkedList<int> list;
    ds2::LinkedList<float> list1;
    ds2::LinkedList<long> list2;
    ds2::LinkedList<double> list3;
    ds2::LinkedList<std::string> list4;
    ds2::LinkedList<A> list5;
}

TEST(LinkedList, empty) {
    ds2::LinkedList<int> list;
    EXPECT_TRUE(list.empty());
    auto it = list.emplace(list.cbegin(), 1);
    EXPECT_FALSE(list.empty());
    list.erase(it);
    EXPECT_TRUE(list.empty());
}

TEST(LinkedList, front) {
    ds2::LinkedList<int> list;
    list.emplace(list.cbegin(), 1);
    EXPECT_EQ(list.front(), 1);
    list.emplace(list.cbegin(), 2);
    EXPECT_EQ(list.front(), 2);
    list.emplace(list.cbegin(), 3);
    EXPECT_EQ(list.front(), 3);
    list.erase(list.begin());
    EXPECT_EQ(list.front(), 2);
    list.erase(list.begin());
    EXPECT_EQ(list.front(), 1);
    EXPECT_EQ(list.erase(list.begin()), list.end());
}

TEST(LinkedList, erase_the_only_element) {
    ds2::LinkedList<int> list;
    auto it1 = list.emplace(list.cbegin(), 1);
    auto it2 = list.erase(it1);
    EXPECT_EQ(it2, list.end());
}

TEST(LinkedList, erase_end) {
    ds2::LinkedList<int> list;
    auto it1 = list.emplace(list.cbegin(), 1);
    auto it2 = list.emplace(list.cbegin(), 2);
    auto it3 = list.erase(it2);
    EXPECT_EQ(it3, list.begin());
}

TEST(LinkedList, iterator) {
    ds2::LinkedList<int> list;
    list.emplace(list.cbegin(), 3);
    list.emplace(list.cbegin(), 2);
    list.emplace(list.cbegin(), 1);

    auto it = list.cbegin();
    EXPECT_EQ(*it, 1);
    ++it;
    EXPECT_EQ(*it, 2);
    ++it;
    EXPECT_EQ(*it, 3);
    --it;
    EXPECT_EQ(*it, 2);
    ++it;
    EXPECT_EQ(*it, 3);
    ++it;
    EXPECT_EQ(it, list.cend());
}
