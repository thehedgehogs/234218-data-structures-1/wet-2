#include "hash_table.hpp"

#include <cmath>
#include <gtest/gtest.h>
#include <unordered_map>

class IntHash {
    public:
        explicit constexpr IntHash(const unsigned long _size) noexcept
            : size{_size} {
        }

        constexpr unsigned long operator()(const int key) const noexcept {
            return static_cast<unsigned long>(key) % size;
        }

    private:
        unsigned long size;
};

class FloatHash {
    public:
        explicit constexpr FloatHash(const unsigned long _size) noexcept
            : size{_size} {
        }

        constexpr unsigned long operator()(const float key) const noexcept {
            return static_cast<unsigned long>(std::lround(key)) % size;
        }

    private:
        unsigned long size;
};

class StringHash {
    public:
        explicit constexpr StringHash(const unsigned long &_size) noexcept
            : size{_size} {
        }

        unsigned long operator()(const std::string &key) const noexcept {
            return static_cast<unsigned long>(key.size()) % size;
        }

    private:
        unsigned long size;
};

TEST(HashTable, construct_and_destruct) {
    ds2::HashTable<int, int, IntHash> hash_table1;
    ds2::HashTable<int, float, IntHash> hash_table2;
    ds2::HashTable<float, int, FloatHash> hash_table3;
    ds2::HashTable<float, float, FloatHash> hash_table4;
    ds2::HashTable<int, std::string, IntHash> hash_table5;
    ds2::HashTable<float, std::string, IntHash> hash_table6;
    ds2::HashTable<std::string, int, StringHash> hash_table7;
    ds2::HashTable<std::string, float, StringHash> hash_table8;
    ds2::HashTable<std::string, std::string, StringHash> hash_table9;
}

/*
 * DEFAULT_CAPACITY = 20
 * RESIZE_FACTOR    = 2
 * SHRINK_BOUNDARY_FACTOR    = 4
 */

TEST(HashTable, empty_size_capacity) {
    ds2::HashTable<int, int, IntHash, std::size_t, 2> ht{2};

    EXPECT_TRUE(ht.empty());
    EXPECT_EQ(ht.size(), 0);
    EXPECT_EQ(ht.capacity(), 2);

    EXPECT_TRUE(ht.emplace(1, 2).second);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 1);
    EXPECT_EQ(ht.capacity(), 2);

    EXPECT_FALSE(ht.emplace(1, 1).second);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 1);
    EXPECT_EQ(ht.capacity(), 2);

    EXPECT_TRUE(ht.emplace(2, 2).second);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 2);
    EXPECT_EQ(ht.capacity(), 2);

    EXPECT_TRUE(ht.emplace(3, 3).second);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 3);
    EXPECT_EQ(ht.capacity(), 4);

    EXPECT_TRUE(ht.emplace(4, 4).second);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 4);
    EXPECT_EQ(ht.capacity(), 4);

    EXPECT_TRUE(ht.emplace(5, 5).second);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 5);
    EXPECT_EQ(ht.capacity(), 8);

    EXPECT_EQ(ht.erase(5), 1);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 4);
    EXPECT_EQ(ht.capacity(), 8);

    EXPECT_EQ(ht.erase(4), 1);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 3);
    EXPECT_EQ(ht.capacity(), 8);

    EXPECT_EQ(ht.erase(4), 0);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 3);
    EXPECT_EQ(ht.capacity(), 8);

    EXPECT_EQ(ht.erase(1), 1);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 2);
    EXPECT_EQ(ht.capacity(), 8);

    EXPECT_EQ(ht.erase(1), 0);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 2);
    EXPECT_EQ(ht.capacity(), 8);

    EXPECT_EQ(ht.erase(99), 0);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 2);
    EXPECT_EQ(ht.capacity(), 8);

    EXPECT_EQ(ht.erase(2), 1);
    EXPECT_FALSE(ht.empty());
    EXPECT_EQ(ht.size(), 1);
    EXPECT_EQ(ht.capacity(), 4);

    EXPECT_EQ(ht.erase(3), 1);
    EXPECT_TRUE(ht.empty());
    EXPECT_EQ(ht.size(), 0);
    EXPECT_EQ(ht.capacity(), 2);

    EXPECT_EQ(ht.erase(1), 0);
    EXPECT_TRUE(ht.empty());
    EXPECT_EQ(ht.size(), 0);
    EXPECT_EQ(ht.capacity(), 2);
}

TEST(HashTable, shrink_expand) {
    ds2::HashTable<int, int, IntHash, std::size_t, 2> ht{2};
    const int upper_bound{100};

    // fill 99 elements, so that capacity == 128
    for (int i{1}; i < upper_bound; ++i) {
        EXPECT_TRUE(ht.emplace(i, i).second);

        // expand 'capacity' if 'i' is power of two
        const std::size_t power{static_cast<std::size_t>(std::floor(std::log2(static_cast<double>(ht.size() - 1))))};
        EXPECT_EQ(ht.capacity(), 2 << power) << "i = " << i << " size = " << ht.size();
    }

    // fill 99 elements, so that capacity == 128
    for (int i{1}; i < upper_bound; ++i) {
        EXPECT_EQ(1, ht.erase(i));

        const std::size_t power{static_cast<std::size_t>(std::floor(1 + std::log2(static_cast<double>(ht.size()))))};
        EXPECT_GE(ht.capacity(), 1 << power) << "i = " << i << "\tsize = " << ht.size();
        EXPECT_LE(ht.capacity(), 2 << power) << "i = " << i << "\tsize = " << ht.size();
    }
}

TEST(HashTable, find) {
    ds2::HashTable<int, int, IntHash> ht;
    auto it = ht.find(1);
    EXPECT_EQ(it, ht.end());

    static constexpr const int factor{2};
    for (int i{0}; i < 10; ++i) {
        EXPECT_TRUE(ht.emplace(i * factor, i * factor).second);
    }
    // [0, nullptr, 2, nullptr, 4, nullptr, 6, nullptr, 8, nullptr]

    for (int i{0}; i < 10 * factor; ++i) {
        it = ht.find(i);
        if (i % factor == 0) {
            EXPECT_EQ((*it).second, i);
        } else {
            EXPECT_EQ(it, ht.end());
        }
    }
}

// TODO: read comment inside
TEST(HashTable, swap) {
    ds2::HashTable<int, int, IntHash> ht1;
    ds2::HashTable<std::string, float, StringHash> ht2;
    // swap(&ht1, &ht2);
    // what is the syntax of swap? where do we even use it?
}

TEST(HashTable, exists) {
    ds2::HashTable<int, int, IntHash> ht;
    auto it = ht.find(1);
    EXPECT_EQ(it, ht.end());

    static constexpr const int factor{2};
    for (int i{0}; i < 10; ++i) {
        EXPECT_TRUE(ht.emplace(i * factor, i * factor).second);
    }
    // [0, nullptr, 2, nullptr, 4, nullptr, 6, nullptr, 8, nullptr]

    for (int i{0}; i < 10 * factor; ++i) {
        it = ht.find(i);
        if (i % factor == 0) {
            EXPECT_TRUE(ht.exists(i));
        } else {
            EXPECT_FALSE(ht.exists(i));
        }
    }
}

TEST(HashTable, exists2) {
    ds2::HashTable<int, int, IntHash> ht;
    EXPECT_TRUE(ht.emplace(10, 1).second);
    EXPECT_FALSE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_FALSE(ht.exists(11));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_FALSE(ht.exists(5555));

    EXPECT_TRUE(ht.emplace(1, 4).second);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_FALSE(ht.exists(11));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_FALSE(ht.exists(5555));

    EXPECT_TRUE(ht.emplace(5555, 1).second);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_FALSE(ht.exists(11));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_TRUE(ht.exists(5555));

    EXPECT_TRUE(ht.emplace(11, 11).second);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_TRUE(ht.exists(11));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_TRUE(ht.exists(5555));

    EXPECT_FALSE(ht.emplace(10, 10).second);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_TRUE(ht.exists(11));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_TRUE(ht.exists(5555));

    EXPECT_FALSE(ht.emplace(5555, 10).second);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_TRUE(ht.exists(11));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_TRUE(ht.exists(5555));

    EXPECT_FALSE(ht.emplace(11, 10).second);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_TRUE(ht.exists(11));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_TRUE(ht.exists(5555));

    EXPECT_TRUE(ht.emplace(69, 1).second);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_TRUE(ht.exists(11));
    EXPECT_TRUE(ht.exists(69));
    EXPECT_TRUE(ht.exists(5555));
}

TEST(HashTable, exists3) {
    ds2::HashTable<int, int, IntHash> ht;
    EXPECT_TRUE(ht.emplace(1, 1).second);
    EXPECT_TRUE(ht.emplace(555, 1).second);
    EXPECT_TRUE(ht.emplace(10, 1).second);
    EXPECT_TRUE(ht.emplace(69, 11).second);

    EXPECT_EQ(ht.erase(11), 0);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_TRUE(ht.exists(10));
    EXPECT_TRUE(ht.exists(69));
    EXPECT_TRUE(ht.exists(555));

    EXPECT_EQ(ht.erase(10), 1);
    EXPECT_TRUE(ht.exists(1));
    EXPECT_FALSE(ht.exists(10));
    EXPECT_TRUE(ht.exists(69));
    EXPECT_TRUE(ht.exists(555));

    EXPECT_EQ(ht.erase(1), 1);
    EXPECT_FALSE(ht.exists(1));
    EXPECT_FALSE(ht.exists(10));
    EXPECT_TRUE(ht.exists(69));
    EXPECT_TRUE(ht.exists(555));

    EXPECT_EQ(ht.erase(69), 1);
    EXPECT_FALSE(ht.exists(1));
    EXPECT_FALSE(ht.exists(10));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_TRUE(ht.exists(555));

    EXPECT_EQ(ht.erase(555), 1);
    EXPECT_FALSE(ht.exists(1));
    EXPECT_FALSE(ht.exists(10));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_FALSE(ht.exists(555));

    EXPECT_EQ(ht.erase(555), 0);
    EXPECT_FALSE(ht.exists(1));
    EXPECT_FALSE(ht.exists(10));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_FALSE(ht.exists(555));

    EXPECT_TRUE(ht.emplace(555, 5).second);
    EXPECT_FALSE(ht.exists(1));
    EXPECT_FALSE(ht.exists(10));
    EXPECT_FALSE(ht.exists(69));
    EXPECT_TRUE(ht.exists(555));
}

TEST(HashTable, iterator) {
    ds2::HashTable<std::string, int, StringHash> ht;
    std::unordered_map<std::string, int> values{{"fear", 1}, {"of", 2}, {"a", 3}, {"blank", 4}, {"planet", 5}};

    for (const auto &pair : values) {
        ASSERT_EQ(1, ht.emplace(pair.first, pair.second).second);
    }

    for (const auto &pair : ht) {
        EXPECT_EQ(values.at(pair.first), pair.second);
        values.erase(pair.first);
    }
    EXPECT_TRUE(values.empty());
}

TEST(HashTable, iterator2) {
    ds2::HashTable<std::string, int, StringHash> ht;
    std::unordered_map<std::string, int> values{
        {"fear", 1}, {"of", 2}, {"a", 3}, {"blank", 4}, {"planet", 5}, {"aa", 6}, {"ab", 7}};

    for (const auto &pair : values) {
        ASSERT_EQ(1, ht.emplace(pair.first, pair.second).second);
    }

    ASSERT_EQ(1, ht.erase("of"));
    values.erase("of");

    ASSERT_EQ(1, ht.erase("a"));
    values.erase("a");

    for (const auto &pair : ht) {
        EXPECT_EQ(values.at(pair.first), pair.second);
        values.erase(pair.first);
    }

    EXPECT_TRUE(values.empty());
}

TEST(HashTable, merge) {
    ds2::HashTable<int, int, IntHash> ht1;
    ds2::HashTable<int, int, IntHash> ht2;

    std::unordered_map<int, int> map1;
    std::unordered_map<int, int> map2;

    ASSERT_TRUE(ht1.emplace(1, 100).second);
    ASSERT_TRUE(ht1.emplace(5, 100).second);
    ASSERT_TRUE(ht1.emplace(9, 100).second);
    ASSERT_TRUE(ht1.emplace(3, 100).second);
    ASSERT_TRUE(ht1.emplace(8, 100).second);

    ASSERT_TRUE(ht2.emplace(1, 200).second);
    ASSERT_TRUE(ht2.emplace(5, 200).second);
    ASSERT_TRUE(ht2.emplace(2, 200).second);
    ASSERT_TRUE(ht2.emplace(7, 200).second);
    ASSERT_TRUE(ht2.emplace(8, 200).second);
    ASSERT_TRUE(ht2.emplace(6, 200).second);


    ASSERT_TRUE(map1.emplace(1, 100).second);
    ASSERT_TRUE(map1.emplace(5, 100).second);
    ASSERT_TRUE(map1.emplace(9, 100).second);
    ASSERT_TRUE(map1.emplace(3, 100).second);
    ASSERT_TRUE(map1.emplace(8, 100).second);

    ASSERT_TRUE(map2.emplace(1, 200).second);
    ASSERT_TRUE(map2.emplace(5, 200).second);
    ASSERT_TRUE(map2.emplace(2, 200).second);
    ASSERT_TRUE(map2.emplace(7, 200).second);
    ASSERT_TRUE(map2.emplace(8, 200).second);
    ASSERT_TRUE(map2.emplace(6, 200).second);

    ht1.merge(ht2);

    for (const auto &pair : ht1) {
        ASSERT_TRUE((end(map1) != map1.find(pair.first)) || (end(map2) != map2.find(pair.first)));
        if (end(map1) != map1.find(pair.first)) {
            ASSERT_EQ(map1.at(pair.first), pair.second);
            map1.erase(pair.first);
            map2.erase(pair.first);
        } else if (end(map2) != map2.find(pair.first)) {
            ASSERT_EQ(map2.at(pair.first), pair.second);
            map2.erase(pair.first);
        }
    }
    ASSERT_TRUE(map1.empty());
    ASSERT_TRUE(map2.empty());
}
