#include "avl_tree.hpp"

#include <gtest/gtest.h>

struct A {
        int data;
};

struct ExtractA {
    public:
        using Abelian = int;

        int operator()(const A &a) const noexcept {
            return a.data;
        }
};

template<typename T>
struct Identity {
    public:
        using Abelian = T;

        const T &operator()(const T &value) const noexcept {
            return value;
        }
};

template<typename V, V value, typename T = V>
struct Constant {
    public:
        using Abelian = V;

        const V operator()(__attribute__((unused)) const T &ignored) const noexcept {
            return value;
        }
};

template<typename T, typename V>
using RankTree = ds2::AVLTree<T, V, Constant<V, 1>>;

TEST(AVLTree, construct_and_destruct) {
    ds2::AVLTree<int, int, Identity<int>> tree1;
    ds2::AVLTree<int, int, Constant<int, 1>> tree2;
    ds2::AVLTree<int, float, Constant<int, 1, float>> tree3;
    ds2::AVLTree<int, A, ExtractA> tree4;

    ds2::AVLTree<int, int, Identity<int>, long> tree5;
    ds2::AVLTree<int, int, Constant<int, 1, float>, long> tree6;
    ds2::AVLTree<int, float, Constant<int, 1, float>, long> tree7;
    ds2::AVLTree<int, A, ExtractA, long> tree8;
}

TEST(AVLTree, int_sum_type) {
    ds2::AVLTree<int, int, Identity<int>> tree;
    for (int i = 1; i < 10; ++i) {
        tree.emplace(i, 10 * i);
    }
    for (int i = 1; i < 10; ++i) {
        auto it = tree.find_by_rank(static_cast<unsigned long>(i));
        EXPECT_EQ(it->first, i);
        EXPECT_EQ(it->second, 10 * i);
    }
}

TEST(AVLTree, sum_over_constant_is_rank) {
    using Tree = ds2::AVLTree<int, int, Constant<int, 1>>;
    Tree tree{};

    // far from a full binary tree
    const int far_from_a_power_of_two{10};
    for (int i{1}; i < far_from_a_power_of_two; ++i) {
        tree.emplace(i, i);
    }

    for (const Tree::value_type &pair : tree) {
        EXPECT_EQ(tree.rank(pair.first), pair.first);
        EXPECT_EQ(tree.sum(pair.first), pair.first);
    }
}

TEST(AVLTree, sum_over_constant_is_a_multiple_of_rank) {
    const int factor{17};
    using Tree = ds2::AVLTree<int, int, Constant<int, factor>>;
    Tree tree{};

    // far from a full binary tree
    const int far_from_a_power_of_two{10};
    for (int i{1}; i < far_from_a_power_of_two; ++i) {
        tree.emplace(i, i);
    }

    for (const Tree::value_type &pair : tree) {
        EXPECT_EQ(tree.rank(pair.first), pair.first);
        EXPECT_EQ(tree.sum(pair.first), factor * pair.first);
    }
}

TEST(AVLTree, sum_over_identity) {
    using Tree = ds2::AVLTree<int, int, Identity<int>>;
    Tree tree{};

    // far from a full binary tree
    const int far_from_a_power_of_two{10};
    for (int i{1}; i < far_from_a_power_of_two; ++i) {
        tree.emplace(i, i);
    }

    for (const Tree::value_type &pair : tree) {
        EXPECT_EQ(tree.sum(pair.first), pair.first * (pair.first + 1) / 2);
    }
}

TEST(AVLTree, lower_bound) {
    RankTree<int, int> tree{};
    tree.emplace(4, 4);
    tree.emplace(2, 2);
    tree.emplace(6, 6);

    EXPECT_EQ(tree.lower_bound(0)->first, 2);
    EXPECT_EQ(tree.lower_bound(1)->first, 2);
    EXPECT_EQ(tree.lower_bound(2)->first, 2);
    EXPECT_EQ(tree.lower_bound(3)->first, 4);
    EXPECT_EQ(tree.lower_bound(4)->first, 4);
    EXPECT_EQ(tree.lower_bound(5)->first, 6);
    EXPECT_EQ(tree.lower_bound(6)->first, 6);
    EXPECT_EQ(tree.lower_bound(7), tree.cend());
}

TEST(AVLTree, upper_bound) {
    RankTree<int, int> tree{};
    tree.emplace(4, 4);
    tree.emplace(2, 2);
    tree.emplace(6, 6);

    EXPECT_EQ(tree.upper_bound(0)->first, 2);
    EXPECT_EQ(tree.upper_bound(1)->first, 2);
    EXPECT_EQ(tree.upper_bound(2)->first, 4);
    EXPECT_EQ(tree.upper_bound(3)->first, 4);
    EXPECT_EQ(tree.upper_bound(4)->first, 6);
    EXPECT_EQ(tree.upper_bound(5)->first, 6);
    EXPECT_EQ(tree.upper_bound(6), tree.cend());
    EXPECT_EQ(tree.upper_bound(7), tree.cend());
}

TEST(AVLTree, lower_bound_reversed) {
    RankTree<int, int> tree{};
    tree.emplace(4, 4);
    tree.emplace(2, 2);
    tree.emplace(6, 6);

    EXPECT_EQ(tree.lower_bound_reversed(0), tree.cend());
    EXPECT_EQ(tree.lower_bound_reversed(1), tree.cend());
    EXPECT_EQ(tree.lower_bound_reversed(2)->first, 2);
    EXPECT_EQ(tree.lower_bound_reversed(3)->first, 2);
    EXPECT_EQ(tree.lower_bound_reversed(4)->first, 4);
    EXPECT_EQ(tree.lower_bound_reversed(5)->first, 4);
    EXPECT_EQ(tree.lower_bound_reversed(6)->first, 6);
    EXPECT_EQ(tree.lower_bound_reversed(7)->first, 6);
}

TEST(AVLTree, upper_bound_reversed) {
    RankTree<int, int> tree{};
    tree.emplace(4, 4);
    tree.emplace(2, 2);
    tree.emplace(6, 6);

    EXPECT_EQ(tree.upper_bound_reversed(0), tree.cend());
    EXPECT_EQ(tree.upper_bound_reversed(1), tree.cend());
    EXPECT_EQ(tree.upper_bound_reversed(2), tree.cend());
    EXPECT_EQ(tree.upper_bound_reversed(3)->first, 2);
    EXPECT_EQ(tree.upper_bound_reversed(4)->first, 2);
    EXPECT_EQ(tree.upper_bound_reversed(5)->first, 4);
    EXPECT_EQ(tree.upper_bound_reversed(6)->first, 4);
    EXPECT_EQ(tree.upper_bound_reversed(7)->first, 6);
}

TEST(AVLTree, float_sum_type) {
    ds2::AVLTree<int, float, Constant<int, 1, float>> tree;
}

TEST(AVLTree, A_sum_type) {
    ds2::AVLTree<int, A, ExtractA> tree;
}

TEST(AVLTree, sum_is_int_grade_is_long) {
    ds2::AVLTree<int, int, Identity<int>, long> tree;
}
