#ifndef WET_2_AVL_TREE_HPP
#define WET_2_AVL_TREE_HPP

#include "pair.hpp"

#include <cassert>
#include <cmath>
#include <cstddef>
#include <memory>

#define assertm(assumption, message) assert((assumption) || (nullptr == message))

namespace ds2 {

template<typename T>
using owner = T;

template<typename K, typename T, typename ExtractAbelian, typename Abelian = typename ExtractAbelian::Abelian>
class AVLTree {
    private:
        template<typename V>
        class raw_iterator;
        struct Node;

    public:
        using size_type = std::size_t;
        using mapped_type = T;
        using key_type = K;
        using sum_type = Abelian;
        using value_type = pair<const key_type, mapped_type>;
        using iterator = raw_iterator<value_type>;
        using const_iterator = raw_iterator<const value_type>;

        AVLTree() = default;

        ~AVLTree() {
            remove_subtree(&root);
        }

        AVLTree(const AVLTree &other)
            : root{copy_subtree(other.root)}
            , elements{other.elements} {
        }

        AVLTree(AVLTree &&other) noexcept {
            swap(*this, other);
        }

        AVLTree &operator=(AVLTree other) noexcept {
            swap(*this, other);
            return *this;
        }

        constexpr bool empty() const noexcept {
            return nullptr == root;
        }

        constexpr std::size_t size() const noexcept {
            return elements;
        }

        constexpr sum_type sum() const noexcept {
            return _sum;
        }

        // lower than or equal to target
        const_iterator lower_bound_reversed(const key_type &target) const noexcept {
            Node *const closest{find_closest_node_inner(target).second};
            if (nullptr == closest) {
                return end();
            } else if (!(target < closest->value.first)) {
                // closest <= target
                assert((nullptr == closest->successor()) || (target < closest->successor()->value.first));
                return const_iterator{closest};
            } else {
                // target < closest
                assert(target < closest->value.first);
                Node *const previous{closest->predecessor()};
                assert((nullptr == previous) || (previous->value.first < target));
                return const_iterator{previous};
            }
        }

        // strictly lower than target
        const_iterator upper_bound_reversed(const key_type &target) const noexcept {
            Node *const closest{find_closest_node_inner(target).second};
            if (nullptr == closest) {
                return end();
            } else if (closest->value.first < target) {
                assert((nullptr == closest->successor()) || (!(closest->successor()->value.first < target)));
                return const_iterator{closest};
            } else {
                // target <= closest
                assert(!(closest->value.first < target));
                Node *const previous{closest->predecessor()};
                assert((nullptr == previous) || (!(target < previous->value.first)));
                return const_iterator{previous};
            }
        }

        // greater than or equal to target
        const_iterator lower_bound(const key_type &target) const noexcept {
            Node *const closest{find_closest_node_inner(target).second};
            if (nullptr == closest) {
                return end();
            } else if (!(closest->value.first < target)) {
                assert((nullptr == closest->predecessor()) || (closest->predecessor()->value.first < target));
                return const_iterator{closest};
            } else {
                // target >= closest
                assert(!(target < closest->value.first));
                Node *const next{closest->successor()};
                assert((nullptr == next) || (target < next->value.first));
                return const_iterator{next};
            }
        }

        // strictly greater than target
        const_iterator upper_bound(const key_type &target) const noexcept {
            Node *const closest{find_closest_node_inner(target).second};
            if (nullptr == closest) {
                return end();
            } else if (target < closest->value.first) {
                assert((nullptr == closest->predecessor()) || !(target < closest->predecessor()->value.first));
                return const_iterator{closest};
            } else {
                // target >= closest
                assert(!(target < closest->value.first));
                Node *const next{closest->successor()};
                assert((nullptr == next) || (target < next->value.first));
                return const_iterator{next};
            }
        }

        const_iterator find(const key_type &target) const noexcept {
            Node *closest{find_closest_node_inner(target).second};
            if ((nullptr != closest) && (target == closest->value.first)) {
                return const_iterator{closest};
            } else {
                return end();
            }
        }

        // TODO: avoid duplication
        iterator find(const key_type &target) noexcept {
            Node *closest{find_closest_node_inner(target).second};
            if ((nullptr != closest) && (target == closest->value.first)) {
                return iterator{closest};
            } else {
                return end();
            }
        }

        iterator find_by_rank(const size_type target) noexcept {
            return iterator{find_by_rank_unsafe(target)};
        }

        const_iterator find_by_rank(const size_type target) const noexcept {
            return const_iterator{find_by_rank_unsafe(target)};
        }

        size_type rank(const K &key) const noexcept {
            const pair<pair<size_type, sum_type>, Node *> result = find_closest_node_inner(key);
            return result.second->value.first == key ? result.first.first : 0;
        }

        sum_type sum(const K &key) const noexcept {
            const pair<pair<size_type, sum_type>, Node *> result = find_closest_node_inner(key);
            return result.second->value.first == key ? result.first.second : 0;
        }

        void adjust_sum(const iterator target, const sum_type &adjustment) noexcept {
            assert(end() != target);
            target.node->adjustment += adjustment;
            _sum += adjustment;
        }

        const_iterator cbegin() const {
            return const_iterator{find_bottom(root)};
        }

        const_iterator begin() const {
            return const_iterator{find_bottom(root)};
        }

        const_iterator cend() const {
            return const_iterator{nullptr};
        }

        const_iterator end() const {
            return const_iterator{nullptr};
        }

        // TODO: avoid duplication
        iterator begin() {
            return iterator{find_bottom(root)};
        }

        iterator end() {
            return iterator{nullptr};
        }

        size_type erase(const key_type &target) noexcept {
            Node *closest{find_closest_node_inner(target).second};
            if ((nullptr == closest) || (target != closest->value.first)) {
                return 0;
            }

            assertm(target == closest->value.first, "expected to find target node");
            erase(iterator{closest});
            return 1;
        }

        iterator erase(iterator target) noexcept {
            if (end() == target) {
                return end();
            }

            // for later
            iterator result{target};
            ++result;

            const owner<Node *> to_remove{target.node};
            assert(nullptr != to_remove);

            // two children
            if ((nullptr != to_remove->left) && (nullptr != to_remove->right)) {
                swap_with_successor_before_removal(to_remove);
            }

            // at most one children
            assert((nullptr == to_remove->left) || (nullptr == to_remove->right));
            Node *const parent{to_remove->parent};

            if ((nullptr == to_remove->left) && (nullptr == to_remove->right)) {  // no children
                if (nullptr == parent) {  // root
                    assert((to_remove == root) && (1 == elements) && (to_remove == find_bottom(root)));
                    root = nullptr;
                } else {  // not root
                    assert(nullptr != parent);
                    if (to_remove == parent->right) {
                        parent->right = nullptr;
                    } else {
                        parent->left = nullptr;
                    }
                    to_remove->parent = nullptr;
                }
            } else {  // exactly one child
                assert((nullptr != to_remove->left) ^ (nullptr != to_remove->right));
                Node *const to_replace_with{nullptr == to_remove->left ? to_remove->right : to_remove->left};
                to_remove->left = nullptr;
                to_remove->right = nullptr;
                if (nullptr != parent) {  // not root
                    const bool is_right{to_remove == parent->right};
                    if (is_right) {
                        parent->link_right(to_replace_with);
                    } else {
                        parent->link_left(to_replace_with);
                    }
                    to_remove->parent = nullptr;
                } else {  // root
                    assert(to_remove == root);
                    root = to_replace_with;
                    assert(nullptr != root);
                    to_replace_with->parent = nullptr;
                }
            }

            assert((nullptr == to_remove->parent) && (nullptr == to_remove->left) && (nullptr == to_remove->right));
            _sum -= (to_remove->subtree_sum + to_remove->adjustment);
            delete to_remove;
            --elements;
            balance_after_remove(parent);
            return result;
        }

        template<typename... Args>
        pair<iterator, bool> emplace(Args &&...args) {
            owner<Node *> to_add{new Node{value_type{std::forward<Args>(args)...}}};
            const key_type &key = to_add->value.first;
            Node *existing{find_closest_node_inner(key).second};
            if (nullptr == existing) {
                assertm(nullptr == root, "root is closer than nothing");
                root = to_add;
                assert(0 == size());
                assert(0 == sum());
                ++elements;
                _sum += to_add->subtree_sum;
                return pair<iterator, bool>{iterator{root}, true};
            } else if (key == existing->value.first) {
                delete to_add;
                // existing->value.second =
                // std::move(value_type{std::move(args)...}.second);
                return pair<iterator, bool>{iterator{existing}, false};
            }
            if (key < existing->value.first) {
                assertm(nullptr == existing->left, "there are smaller fish to fry");
                existing->link_left(to_add);
            } else {
                assert(existing->value.first < key);
                assertm(nullptr == existing->right, "there are bigger fish to fry");
                existing->link_right(to_add);
            }
            balance_after_insert(to_add);
            ++elements;
            _sum += to_add->subtree_sum;
            return pair<iterator, bool>{iterator{to_add}, true};
        }

        void merge(AVLTree &&other) {
            if (other.empty()) {
                return;
            } else if (empty()) {
                swap(*this, other);
                return;
            }

            pair<std::size_t, std::unique_ptr<Node *[]>> p = flatten(std::move(*this), std::move(other));
            const std::size_t length{p.first};
            std::unique_ptr<Node *[]> all_pairs { std::move(p.second) };

            const std::size_t perfect_tree{h_to_n(n_to_h(length) - 1)};
            assert(perfect_tree <= length);
            // only even indices count
            const std::size_t additions{2 * (length - perfect_tree)};

            std::unique_ptr<Node *[]> perfect_nodes { new Node *[perfect_tree] };
            std::unique_ptr<Node *[]> additional_nodes { new Node *[length - perfect_tree] };

            std::size_t i_additions{0};
            std::size_t i_perfect{0};
            for (std::size_t i{0}; i < length; ++i) {
#ifndef NDEBUG
                if (i < length - 1) {
                    assert(all_pairs[i]->value.first < all_pairs[i + 1]->value.first);
                }
#endif
                if ((i < additions) && (i % 2 == 0)) {
                    additional_nodes[i_additions] = all_pairs[i];
                    ++i_additions;
                } else {
                    perfect_nodes[i_perfect] = all_pairs[i];
                    ++i_perfect;
                }
                all_pairs[i] = nullptr;
            }
            assert(length == i_additions + i_perfect);
            assert(length - perfect_tree == i_additions);

            AVLTree result{};
            // ignoring the bottom row, the almost-perfect tree is a perfect tree
            result.root = build_perfect_tree(&result.elements, i_perfect, perfect_nodes.get());

            // add bottom row
            for (std::size_t i{0}; i < i_additions; ++i) {
                if (i % 2 == 0) {
                    perfect_nodes[2 * (i / 2)]->link_left(additional_nodes[i]);
                } else {
                    perfect_nodes[2 * (i / 2)]->link_right(additional_nodes[i]);
                }
                ++result.elements;
            }

            refresh_height_subtree(result.root);
            swap(*this, result);
        }

        pair<iterator, bool> insert(value_type &&value) {
            return emplace(std::move(value.first), std::move(value.second));
        }

        pair<iterator, bool> insert(const value_type &value) {
            return emplace(value.first, value.second);
        }

        friend void swap(AVLTree &left, AVLTree &right) {
            using std::swap;
            swap(left.root, right.root);
            swap(left.elements, right.elements);
        }

    private:
        struct Node {
                explicit Node(value_type value_) noexcept
                    : value(value_) {

                    refresh_subtree_properties();
                }

                ~Node() = default;
                Node() = delete;
                Node(const Node &other) = delete;

                Node(Node &&other) noexcept {
                    swap(*this, other);
                }

                Node &operator=(Node other) noexcept {
                    swap(*this, other);
                    return *this;
                }

                void link_right(Node *other) noexcept {
                    if (nullptr != other) {
                        other->parent = this;
                    }
                    right = other;
                }

                void link_left(Node *other) noexcept {
                    if (nullptr != other) {
                        other->parent = this;
                    }
                    left = other;
                }

                Node *successor() noexcept {
                    if (nullptr != right) {
                        Node *current = right;
                        Node *next = current->left;
                        while (nullptr != next) {
                            current = next;
                            next = next->left;
                        }
                        return current;
                    }
                    if (nullptr != parent) {
                        if (this == parent->left) {
                            return parent;
                        }
                        assert(this == parent->right);
                        Node *current = this;
                        Node *next = current->parent;
                        while ((nullptr != next) && (current == next->right)) {
                            current = next;
                            next = next->parent;
                        }
                        if ((nullptr != next) && (current == next->left)) {
                            return next;
                        }
                        assertm(nullptr == next, "supposedly reached the end of the tree");
                        return nullptr;
                    }
                    return nullptr;
                }

                Node *predecessor() noexcept {
                    if (nullptr != left) {
                        Node *current = left;
                        Node *next = current->right;
                        while (nullptr != next) {
                            current = next;
                            next = next->right;
                        }
                        return current;
                    }
                    if (nullptr != parent) {
                        if (this == parent->right) {
                            return parent;
                        }
                        assert(this == parent->left);
                        Node *current = this;
                        Node *next = current->parent;
                        while ((nullptr != next) && (current == next->left)) {
                            current = next;
                            next = next->parent;
                        }
                        if ((nullptr != next) && (current == next->right)) {
                            return next;
                        }
                        assertm(nullptr == next, "supposedly reached the end of the tree");
                        return nullptr;
                    }
                    return nullptr;
                }

                static constexpr size_type get_subtree_size(const Node *const node) noexcept {
                    return nullptr == node ? 0 : node->subtree_size;
                }

                static constexpr sum_type get_subtree_sum(const Node *const node) noexcept {
                    return nullptr == node ? 0 : node->subtree_sum + node->adjustment;
                }

                void refresh_subtree_properties() noexcept {
                    height = 1 + std::max(get_height(left), get_height(right));
                    subtree_size = 1 + get_subtree_size(left) + get_subtree_size(right);
                    subtree_sum = ExtractAbelian{}(value.second) + get_subtree_sum(left) + get_subtree_sum(right);
                }

                static constexpr size_type rank(const Node *const node) noexcept {
                    return nullptr == node ? 0 : get_subtree_size(node) - get_subtree_size(node->right);
                }

                static constexpr sum_type sum(const Node *const node) noexcept {
                    return nullptr == node ? 0 : get_subtree_sum(node) - get_subtree_sum(node->right);
                }

                value_type value;
                Node *left{};
                Node *right{};
                Node *parent{};
                long height{};
                size_type subtree_size{};
                sum_type subtree_sum{};
                sum_type adjustment{};
        };

        template<typename V>
        class raw_iterator {
            public:
                constexpr raw_iterator() noexcept = default;

                constexpr explicit raw_iterator(Node *node_) noexcept
                    : node{node_} {
                }

                constexpr raw_iterator(const raw_iterator &other) noexcept = default;

                constexpr raw_iterator(raw_iterator &&other) noexcept = default;

                constexpr explicit operator const_iterator() const noexcept {
                    return const_iterator{node};
                }

                raw_iterator &operator=(raw_iterator other) noexcept {
                    swap(*this, other);
                    return *this;
                }

                V &operator*() const {
                    assertm(nullptr != node, "raw_iterator out of range");
                    return node->value;
                }

                V &operator*() {
                    assertm(nullptr != node, "raw_iterator out of range");
                    return node->value;
                }

                V *operator->() const {
                    assertm(nullptr != node, "raw_iterator out of range");
                    return &node->value;
                }

                V *operator->() {
                    assertm(nullptr != node, "raw_iterator out of range");
                    return &node->value;
                }

                raw_iterator &operator++() noexcept {
                    node = node->successor();
                    return *this;
                }

                raw_iterator operator++(int) const noexcept {
                    raw_iterator successor{*this};
                    return ++successor;
                }

                raw_iterator &operator--() noexcept {
                    node = node->predecessor();
                    return *this;
                }

                raw_iterator operator--(int) const noexcept {
                    raw_iterator predecessor{*this};
                    return --predecessor;
                }

                friend constexpr bool operator==(const raw_iterator &left, const raw_iterator &right) {
                    return left.node == right.node;
                }

                friend constexpr bool operator!=(const raw_iterator &left, const raw_iterator &right) {
                    return left.node != right.node;
                }

                friend class AVLTree;

                friend void swap(raw_iterator &left, raw_iterator &right) noexcept {
                    using std::swap;
                    swap(left.node, right.node);
                }

            private:
                Node *node{};
        };

        pair<pair<size_type, sum_type>, Node *> find_closest_node_inner(const key_type &target) const noexcept {
            Node *previous{nullptr};
            Node *current{root};
            size_type current_rank{Node::rank(root)};
            sum_type current_sum{Node::sum(root)};

            while (nullptr != current) {
                if (target == current->value.first) {
                    return pair<pair<size_type, sum_type>, Node *>{pair<size_type, sum_type>{current_rank, current_sum},
                                                                   current};
                }

                previous = current;
                if (target < current->value.first) {
                    current_rank = current_rank - Node::rank(current) + Node::rank(current->left);
                    current_sum = current_sum - Node::sum(current) + Node::sum(current->left);
                    current = current->left;
                } else {
                    current_rank += Node::rank(current->right);
                    current_sum += Node::sum(current->right);
                    current = current->right;
                }
            }
            assertm((nullptr == current) || (nullptr == previous)
                        || ((target < previous->value.first) && (previous->predecessor()->value.first < target))
                        || ((previous->value.first < target) && (target < previous->successor()->value.first)),
                    "missed target");
            return pair<pair<size_type, sum_type>, Node *>{
                pair<size_type, sum_type>{Node::rank(previous) + current_rank, Node::sum(previous) + current_sum},
                previous};
        }

        /// O(log(n))
        static Node *find_bottom(Node *const root) {
            Node *smallest = root;
            while ((nullptr != smallest) && (nullptr != smallest->left)) {
                assert(smallest->left->parent == smallest);
                smallest = smallest->left;
            }
            return smallest;
        }

        /// O(n)
        static Node *copy_subtree(const Node *const root) {
            if (nullptr == root) {
                return nullptr;
            }
            owner<Node *> clone{new Node{root->value}};
            clone->link_left(copy_subtree(root->left));
            clone->link_right(copy_subtree(root->right));
            return clone;
        }

        /// O(n)
        static void remove_subtree(owner<Node *> *const root) {
            assert(nullptr != root);
            if (nullptr == *root) {
                return;
            }
            remove_subtree(&(*root)->left);
            remove_subtree(&(*root)->right);
            delete *root;
            *root = nullptr;
        }

        /// O(n)
        static void refresh_height_subtree(Node *const root) {
            if (nullptr == root) {
                return;
            }
            refresh_height_subtree(root->left);
            refresh_height_subtree(root->right);
            root->refresh_subtree_properties();
        }

        static long get_height(const Node *node) noexcept {
            return static_cast<long>(nullptr == node ? -1 : node->height);
        }

        static long balance_factor(const Node *node) noexcept {
            return nullptr == node ? 0 : get_height(node->left) - get_height(node->right);
        }

        void rotate_right_right(Node *const b) noexcept {
            // destruct to nodes
            if (nullptr != b->parent) {
                Node *const above{b->parent};
                const bool is_right{above->right == b};
                Node *const a{b->right};
                Node *const a_l{a->left};

                // switcheroo
                if (is_right) {
                    above->link_right(a);
                } else {
                    above->link_left(a);
                }
                b->link_right(a_l);
                a->link_left(b);
            } else {
                assert(b == root);
                Node *const a{b->right};
                Node *const a_l{a->left};

                // switcheroo
                root = a;
                a->parent = nullptr;
                b->link_right(a_l);
                a->link_left(b);
            }
            b->refresh_subtree_properties();
            b->parent->refresh_subtree_properties();
        }

        void rotate_right_left(Node *const c) noexcept {
            rotate_left_left(c->right);
            rotate_right_right(c);
        }

        void rotate_left_right(Node *const c) noexcept {
            rotate_right_right(c->left);
            rotate_left_left(c);
        }

        void rotate_left_left(Node *const b) noexcept {
            // destruct to nodes
            if (nullptr != b->parent) {
                Node *const above{b->parent};
                const bool is_right{above->right == b};
                Node *const a{b->left};
                Node *const a_r{a->right};

                // switcheroo
                if (is_right) {
                    above->link_right(a);
                } else {
                    above->link_left(a);
                }
                b->link_left(a_r);
                a->link_right(b);
            } else {
                assert(b == root);
                Node *const a{b->left};
                Node *const a_r{a->right};

                // switcheroo
                root = a;
                a->parent = nullptr;
                b->link_left(a_r);
                a->link_right(b);
            }
            b->refresh_subtree_properties();
            b->parent->refresh_subtree_properties();
        }

        void rotate_left(Node *const node) noexcept {
            const long factor{balance_factor(node->left)};
            if (0 > factor) {
                rotate_left_right(node);
            } else {
                rotate_left_left(node);
            }
        }

        void rotate_right(Node *node) noexcept {
            const long factor{balance_factor(node->right)};
            if (0 < factor) {
                rotate_right_left(node);
            } else {
                rotate_right_right(node);
            }
        }

        bool rebalance(Node *const node) noexcept {
            node->refresh_subtree_properties();
            const long factor{balance_factor(node)};
            // see lecture 4 slide 25
            if (1 < factor) {
                assert(2 == factor);
                rotate_left(node);
                return true;
            } else if (-1 > factor) {
                assert(-2 == factor);
                rotate_right(node);
                return true;
            }
            return false;
        }

        void balance_after_remove(Node *node) noexcept {
            while (nullptr != node) {
                rebalance(node);
                node = node->parent;
            }
        }

        void balance_after_insert(Node *node) noexcept {
            assertm((nullptr != node) && (0 == node->height), "new node should be of height 0");
            while ((nullptr != node) && rebalance(node)) {
                node = node->parent;
            }
            while (nullptr != node) {
                node->refresh_subtree_properties();
                node = node->parent;
            }
        }

        void swap_with_successor_before_removal(Node *const to_remove) noexcept {
            assert((nullptr != to_remove->left) && (nullptr != to_remove->right));

            // BUGGGGGGGGGGGGG ??????????
            Node *target{to_remove->successor()};
            while ((nullptr != target->left) && (nullptr != target->right)) {
                target = target->successor();
            }
            Node *const to_swap_with{target};

            assert(to_swap_with == target);
            assert(nullptr != to_swap_with);
            assert(to_swap_with != to_remove);
            assert(nullptr == to_swap_with->left);

            Node *const to_remove_left{to_remove->left};
            Node *const to_remove_right{to_remove->right};

            Node *const to_swap_with_left{to_swap_with->left};
            Node *const to_swap_with_right{to_swap_with->right};

            Node *const to_swap_with_parent{to_swap_with->parent};
            const bool is_swap_with_right{to_swap_with == to_swap_with_parent->right};

            if (to_swap_with == to_remove->right) {  // handle ajdacent
                if (nullptr == to_remove->parent) {  // handle root
                    assert(to_remove == root);
                    root = to_swap_with;
                    to_swap_with->parent = nullptr;
                } else {  // handle not root
                    assert(to_remove != root);
                    assert(nullptr != to_swap_with->parent);

                    Node *const to_remove_parent{to_remove->parent};
                    const bool is_to_remove_right{to_remove == to_remove->parent->right};
                    if (is_to_remove_right) {
                        to_remove_parent->link_right(to_swap_with);
                    } else {
                        to_remove_parent->link_left(to_swap_with);
                    }
                }

                to_remove->link_left(to_swap_with_left);
                to_remove->link_right(to_swap_with_right);

                to_swap_with->link_left(to_remove_left);
                to_swap_with->link_right(to_remove);

                assert((to_remove->parent == to_swap_with) && (to_remove == to_remove->parent->right));
            } else {  // handle not adjacent
                if (nullptr == to_remove->parent) {  // handle root
                    assert(to_remove == root);
                    root = to_swap_with;
                    to_swap_with->parent = nullptr;
                } else {  // handle not root
                    assert(to_remove != root);
                    assert(nullptr != to_swap_with->parent);
                    assert(to_swap_with != to_swap_with->parent->right);

                    Node *const to_remove_parent{to_remove->parent};
                    const bool is_to_remove_right{to_remove == to_remove->parent->right};
                    if (is_to_remove_right) {
                        to_remove_parent->link_right(to_swap_with);
                    } else {
                        to_remove_parent->link_left(to_swap_with);
                    }
                }

                if (is_swap_with_right) {
                    to_swap_with_parent->link_right(to_remove);
                } else {
                    to_swap_with_parent->link_left(to_remove);
                }
                to_remove->link_left(to_swap_with_left);
                to_remove->link_right(to_swap_with_right);

                to_swap_with->link_left(to_remove_left);
                to_swap_with->link_right(to_remove_right);
                assert(to_remove->parent != to_swap_with);
            }
        }

        static pair<std::size_t, std::unique_ptr<Node *[]>> flatten(AVLTree &&left, AVLTree &&right) {
            const std::size_t length{left.size() + right.size()};
            std::unique_ptr<Node *[]> all_nodes { new Node *[length] };

            iterator iterator_left{left.begin()};
            iterator iterator_right{right.begin()};
            std::size_t i{0};
            while ((left.end() != iterator_left) && (right.end() != iterator_right)) {
                assert(iterator_left->first != iterator_right->first);
                assert(i < length);
                if (iterator_left->first < iterator_right->first) {
                    all_nodes[i] = iterator_left.node;
                    ++iterator_left;
                } else {
                    all_nodes[i] = iterator_right.node;
                    ++iterator_right;
                }
                ++i;
            }
            while (left.end() != iterator_left) {
                assert(right.end() == iterator_right);
                all_nodes[i] = iterator_left.node;
                ++iterator_left;
                ++i;
            }
            while (right.end() != iterator_right) {
                assert(left.end() == iterator_left);
                all_nodes[i] = iterator_right.node;
                ++iterator_right;
                ++i;
            }
            assert(length == i);

            // flatten
            for (std::size_t j{0}; j < length; ++j) {
                all_nodes[j]->parent = nullptr;
                all_nodes[j]->left = nullptr;
                all_nodes[j]->right = nullptr;
                all_nodes[j]->height = 0;
            }

            assert((nullptr == left.root) || (nullptr == left.root->parent));
            left.root = nullptr;
            left.elements = 0;
            assert((nullptr == right.root) || (nullptr == right.root->parent));
            right.root = nullptr;
            right.elements = 0;
            return pair<std::size_t, std::unique_ptr<Node *[]>> { length, std::move(all_nodes) };
        }

        static Node *build_perfect_tree(std::size_t *const out_elements, const std::size_t length, Node *nodes[]) {

            if (0 == length) {
                *out_elements = 0;
                return nullptr;
            } else if (2 > length) {
                assert(1 == length);
                *out_elements = length;
                return nodes[0];
            }

            // assert the array has exactly 2^h - 1 nodes and thus can be made a
            // perfect tree
            // TODO: compare on \epsilon
            assert(std::log2(length + 1) == std::floor(std::log2(length + 1)));
            const std::size_t subroot{length / 2};
            const std::size_t subsize{length / 2};
            std::size_t elements_left{0};
            std::size_t elements_right{0};
            Node *root{nodes[subroot]};
            root->link_left(build_perfect_tree(&elements_left, subsize, nodes));
            root->link_right(build_perfect_tree(&elements_right, subsize, &nodes[subroot + 1]));
            assert(elements_left == elements_right);
            *out_elements = 1 + elements_left + elements_right;
            return root;
        }

        static constexpr std::size_t n_to_h(std::size_t n) {
            return static_cast<std::size_t>(std::ceil(std::log2(n + 1)) - 1);
        }

        static constexpr std::size_t h_to_n(std::size_t h) {
            return static_cast<std::size_t>(std::pow(2, h + 1) - 1);
        }

        Node *find_by_rank_unsafe(const size_type original_target) const noexcept {
            if (original_target > size()) {
                return nullptr;
            }

            size_type target{original_target};
            Node *current{root};

            while (nullptr != current) {
                assert(nullptr != current);
                const size_type current_rank{Node::rank(current)};
                if (current_rank == target) {
                    return current;
                } else if (current_rank < target) {
                    target -= current_rank;
                    current = current->right;
                } else {
                    assert(current_rank > target);
                    current = current->left;
                }
            }

            // assert(!((nullptr == current) ^ (0 == target)));
            // assert(nullptr != current);
            assert(false);
            return current;
        }

        owner<Node *> root{};
        std::size_t elements{};
        sum_type _sum{};
};

}  // namespace ds2

#endif  // WET_2_AVL_TREE_HPP
