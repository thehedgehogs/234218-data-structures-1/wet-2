#include "library2.h"

#include "management.hpp"

#include <cassert>

#define CALL_METHOD(method, ...)                                                                              \
    do {                                                                                                      \
        try {                                                                                                 \
            return (nullptr == management ? INVALID_INPUT                                                     \
                                          : static_cast<ds2::Management *>(management)->method(__VA_ARGS__)); \
        } catch (const std::bad_alloc &error) {                                                               \
            return ALLOCATION_ERROR;                                                                          \
        }                                                                                                     \
    } while (0)

void *Init(const int companies) {
    return 0 < companies ? static_cast<void *>(new ds2::Management{companies}) : nullptr;
}

StatusType AddEmployee(void *const management, const int employee_id, const int company_id, const int grade) {

    CALL_METHOD(add_employee, employee_id, company_id, grade);
}

StatusType RemoveEmployee(void *const management, const int employee_id) {
    CALL_METHOD(remove_employee, employee_id);
}

StatusType AcquireCompany(void *const management, const int acquirer_id, const int target_id, const double factor) {

    CALL_METHOD(acquire_company, acquirer_id, target_id, factor);
}

StatusType EmployeeSalaryIncrease(void *const management, const int employee_id, const int salary_increase) {

    CALL_METHOD(increase_employee_salary, employee_id, salary_increase);
}

StatusType PromoteEmployee(void *const management, const int employee_id, const int bump_grade) {

    CALL_METHOD(promote_employee, employee_id, bump_grade);
}

StatusType SumOfBumpGradeBetweenTopWorkersByGroup(void *const management, const int company_id, const int m) {

    CALL_METHOD(sum_of_bump_grade_between_top_workers_by_company, company_id, m);
}

StatusType AverageBumpGradeBetweenSalaryByGroup(void *const management,
                                                const int company_id,
                                                const int low_salary,
                                                const int high_salary) {

    CALL_METHOD(average_bump_grade_between_salaries_by_company, company_id, low_salary, high_salary);
}

StatusType CompanyValue(void *const management, const int company_id) {
    CALL_METHOD(company_value, company_id);
}

StatusType
    BumpGradeToEmployees(void *const management, const int low_salary, const int high_salary, const int bump_grade) {

    CALL_METHOD(bump_grade_to_employees, low_salary, high_salary, bump_grade);
}

void Quit(void **const management_p) {
    assert(((nullptr != management_p) && (nullptr != *management_p)) || !"Got null management");
    delete static_cast<ds2::Management *>(*management_p);
    *management_p = nullptr;
}
