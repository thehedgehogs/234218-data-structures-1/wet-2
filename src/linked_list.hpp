#ifndef WET_2_LINKED_LIST_HPP
#define WET_2_LINKED_LIST_HPP

#include <cassert>
#include <utility>

namespace ds2 {

template<typename T>
class LinkedList {
    private:
        template<typename V>
        class raw_iterator;
        struct Node;

    public:
        using size_type = std::size_t;
        using value_type = T;
        using iterator = raw_iterator<T>;
        using const_iterator = raw_iterator<const T>;
        using reference = value_type &;
        using const_reference = const value_type &;

        LinkedList() = default;
        LinkedList(const LinkedList &other) = delete;
        LinkedList(LinkedList &&other) noexcept = delete;
        LinkedList &operator=(const LinkedList &other) = delete;
        LinkedList &operator=(LinkedList &&other) noexcept = delete;

        ~LinkedList() noexcept {
            while (!empty()) {
                erase(begin());
            }
        }

        inline reference front() noexcept {
            return _front->value;
        }

        constexpr const_reference front() const noexcept {
            return _front->value;
        }

        inline reference back() noexcept {
            return _back->value;
        }

        constexpr const_reference back() const noexcept {
            return _back->value;
        }

        inline iterator begin() noexcept {
            return iterator{_front};
        }

        constexpr const_iterator begin() const noexcept {
            return const_iterator{_front};
        }

        constexpr const_iterator cbegin() const noexcept {
            return const_iterator{_front};
        }

        inline iterator end() noexcept {
            return iterator{nullptr};
        }

        constexpr const_iterator end() const noexcept {
            return const_iterator{nullptr};
        }

        constexpr const_iterator cend() const noexcept {
            return const_iterator{nullptr};
        }

        template<typename... Args>
        iterator emplace(const_iterator position, Args &&...args) {
            if (empty()) {
                _front = new Node{T(std::forward<Args>(args)...)};
                _back = _front;
                ++_size;
                return begin();
            }

            assert(cend() != position);

            Node *const predecessor{position.node->previous};
            Node *const successor{position.node};
            Node *const inserted = new Node{T(std::forward<Args>(args)...)};
            inserted->link_previous(predecessor);
            inserted->link_next(successor);

            if (cbegin() == position) {
                _front = inserted;
            }

            if (nullptr == inserted->next) {
                assert(nullptr != inserted->previous);
                _back = inserted;
            }

            ++_size;
            return iterator{inserted};
        }

        template<typename... Args>
        iterator emplace_front(Args &&...args) {
            return emplace(cbegin(), std::forward<Args...>(args...));
        }

        template<typename... Args>
        iterator emplace_back(Args &&...args) {
            return emplace(const_iterator{_back}, std::forward<Args...>(args...));
        }

        iterator erase(iterator position) {
            return unsafe_erase(const_iterator{position.node});
        }

        iterator erase(const_iterator position) {
            return unsafe_erase(position);
        }

        constexpr bool empty() const noexcept {
            return nullptr == _front;
        }

        constexpr size_type size() const noexcept {
            return _size;
        }

        friend void swap(LinkedList &left, LinkedList &right) noexcept {
            using std::swap;
            swap(left._front, right._front);
            swap(left._back, right._back);
            swap(left._size, right._size);
        }

    private:
        struct Node {
                constexpr explicit Node(value_type value_) noexcept
                    : value(value_) {
                }

                ~Node() = default;
                Node() = delete;
                Node(const Node &other) = delete;

                Node(Node &&other) noexcept {
                    swap(*this, other);
                }

                Node &operator=(Node other) noexcept {
                    swap(*this, other);
                    return *this;
                }

                void link_next(Node *const other) noexcept {
                    if (nullptr != other) {
                        other->previous = this;
                    }
                    next = other;
                }

                void link_previous(Node *const other) noexcept {
                    if (nullptr != other) {
                        other->next = this;
                    }
                    previous = other;
                }

                T value;
                Node *next{};
                Node *previous{};
        };

        template<typename V>
        class raw_iterator {
            public:
                constexpr raw_iterator() noexcept = default;

                explicit constexpr raw_iterator(Node *const node_) noexcept
                    : node{node_} {
                }

                constexpr raw_iterator(const raw_iterator &other) noexcept = default;
                constexpr raw_iterator(raw_iterator &&other) noexcept = default;

                ~raw_iterator() = default;

                constexpr explicit operator const_iterator() const noexcept {
                    return const_iterator{node};
                }

                raw_iterator &operator=(raw_iterator other) noexcept {
                    swap(*this, other);
                    return *this;
                }

                V &operator*() const {
                    assert(nullptr != node);
                    return node->value;
                }

                V &operator*() {
                    assert(nullptr != node);
                    return node->value;
                }

                V *operator->() const {
                    assert(nullptr != node);
                    return &node->value;
                }

                V *operator->() {
                    assert(nullptr != node);
                    return &node->value;
                }

                raw_iterator &operator++() noexcept {
                    node = node->next;
                    return *this;
                }

                raw_iterator operator++(int) const noexcept {
                    raw_iterator successor{*this};
                    return ++successor;
                }

                raw_iterator &operator--() noexcept {
                    node = node->previous;
                    return *this;
                }

                raw_iterator operator--(int) const noexcept {
                    raw_iterator predecessor{*this};
                    return --predecessor;
                }

                friend constexpr bool operator==(const raw_iterator &left, const raw_iterator &right) noexcept {
                    return left.node == right.node;
                }

                friend constexpr bool operator!=(const raw_iterator &left, const raw_iterator &right) noexcept {
                    return left.node != right.node;
                }

                friend void swap(raw_iterator &left, raw_iterator &right) noexcept {
                    using std::swap;
                    swap(left.node, right.node);
                }

                friend class LinkedList;

            private:
                Node *node{};
        };

        iterator unsafe_erase(const_iterator position) {
            assert(cend() != position);

            Node *const to_remove{position.node};
            Node *const predecessor{to_remove->previous};
            Node *const successor{to_remove->next};
            if (nullptr != predecessor) {
                predecessor->link_next(successor);
            } else if (nullptr != successor) {
                successor->link_previous(predecessor);
            }

            if (to_remove == _front) {
                _front = to_remove->next;
            }

            if (to_remove == _back) {
                _back = to_remove->previous;
            }

            delete to_remove;
            --_size;

            return iterator{successor};
        }

        Node *_front{};
        Node *_back{};
        size_type _size{};
};

}  // namespace ds2

#endif  // WET_2_LINKED_LIST_HPP