#ifndef WET_2_HASH_TABLE_HPP
#define WET_2_HASH_TABLE_HPP

#include "linked_list.hpp"
#include "pair.hpp"

#include <memory>

namespace ds2 {

template<typename K,
         typename T,
         typename HashFunction,
         typename size_type = std::size_t,
         size_type DEFAULT_CAPACITY_ = 20>
class HashTable {
    private:
        template<typename V>
        class raw_iterator;

    public:
        using value_type = pair<const K, T>;
        using iterator = raw_iterator<value_type>;
        using const_iterator = raw_iterator<const value_type>;
        using reference = value_type &;
        using const_reference = const value_type &;

    private:
        using bucket_type = LinkedList<value_type>;
        using const_bucket_type = const bucket_type;
        using bucket_iterator = typename bucket_type::iterator;
        using const_bucket_iterator = typename const_bucket_type::const_iterator;

    public:
        HashTable(const size_type initial_capacity = DEFAULT_CAPACITY)
            : buckets{new bucket_type[initial_capacity]}
            , _capacity{initial_capacity}
            , _size{0} {
        }

        ~HashTable() = default;

        HashTable(const HashTable &other) = delete;

        HashTable(HashTable &&other) noexcept {
            swap(*this, other);
        }

        HashTable &operator=(HashTable other) noexcept {
            swap(*this, other);
            return *this;
        }

        const T *operator[](const K &key) const noexcept {
            pair<const_bucket_type &, const_bucket_iterator> result{find_unsafe(key)};
            return result.first.end() == result.second ? nullptr : &*result.second;
        }

        T &operator[](const K &key) {
            pair<bucket_type &, bucket_iterator> result = find_unsafe(key);
            if (result.first.end() == result.second) {
                rehash(true);
                ++_size;
                return buckets[hash(key)].emplace_front(pair<const K, T>{key, T{}})->second;
            }
            return result.second->second;
        }

        template<typename... Args>
        pair<iterator, bool> emplace(const K &key, Args... args) {
            pair<bucket_type &, bucket_iterator> original_result = find_unsafe(key);
            if (original_result.first.end() == original_result.second) {
                rehash(true);
                pair<bucket_type &, bucket_iterator> rehashed_result = find_unsafe(key);
                assert(rehashed_result.first.end() == rehashed_result.second);
                ++_size;
                bucket_iterator inserted{
                    rehashed_result.first.emplace_front(pair<const K, T>{key, T{std::forward<Args>(args)...}})};
                return pair<iterator, bool>{iterator{this, hash(key), inserted}, true};
            } else {
                return pair<iterator, bool>{iterator{this, hash(key), original_result.second}, false};
            }
        }

        const_iterator find(const K &key) const noexcept {
            return const_iterator{const_cast<HashTable *>(this)->find(key)};
        }

        iterator find(const K &key) noexcept {
            pair<bucket_type &, bucket_iterator> result = find_unsafe(key);
            return result.first.end() == result.second ? end() : iterator{this, hash(key), result.second};
        }

        size_type erase(const K &key) {
            pair<bucket_type &, bucket_iterator> result = find_unsafe(key);
            if (result.first.end() != result.second) {
                result.first.erase(result.second);
                --_size;
                rehash(false);
                return 1;
            }
            return 0;
        }

        const_iterator cbegin() const noexcept {
            return begin();
        }

        const_iterator begin() const noexcept {
            return const_iterator{const_cast<HashTable *>(this)->begin()};
        }

        iterator begin() noexcept {
            for (size_type i{0}; i < capacity(); ++i) {
                if (!buckets[i].empty()) {
                    assert(buckets[i].begin() != buckets[i].end());
                    return iterator{this, i, buckets[i].begin()};
                }
            }
            return end();
        }

        const_iterator cend() const noexcept {
            return end();
        }

        constexpr const_iterator end() const noexcept {
            return const_iterator{const_cast<HashTable *>(this)->end()};
        }

        iterator end() noexcept {
            return iterator{this, capacity(), buckets[capacity() - 1].end()};
        }

        void merge(const HashTable &other) {
            for (const HashTable::value_type &pair : other) {
                emplace(pair.first, pair.second);
            }
        }

        // O(1) amortized
        bool exists(const K key) const noexcept {
            pair<bucket_type &, bucket_iterator> result{find_unsafe(key)};
            return result.first.end() != result.second;
        }

        constexpr bool empty() const noexcept {
            return 0 == size();
        }

        constexpr size_type size() const noexcept {
            return _size;
        }

        constexpr size_type capacity() const noexcept {
            return _capacity;
        }

        friend void swap(HashTable &left, HashTable &right) noexcept {
            using std::swap;
            swap(left.buckets, right.buckets);
            swap(left._capacity, right._capacity);
            swap(left._size, right._size);
        }

    private:
        template<typename V>
        class raw_iterator {
            public:
                constexpr raw_iterator() noexcept = default;

                constexpr raw_iterator(HashTable *const hashtable_,
                                       const size_type current_bucket_,
                                       const bucket_iterator inside_bucket_) noexcept
                    : hashtable{hashtable_}
                    , current_bucket{current_bucket_}
                    , inside_bucket{inside_bucket_} {
                }

                constexpr raw_iterator(const raw_iterator &other) noexcept = default;
                constexpr raw_iterator(raw_iterator &&other) noexcept = default;

                ~raw_iterator() = default;

                constexpr explicit operator const_iterator() const noexcept {
                    return const_iterator{hashtable, current_bucket, inside_bucket};
                }

                raw_iterator &operator=(raw_iterator other) noexcept {
                    swap(*this, other);
                    return *this;
                }

                V &operator*() const {
                    assert((nullptr != hashtable) && (current_bucket < hashtable->capacity())
                           && (hashtable->buckets[current_bucket].end() != inside_bucket));
                    return *inside_bucket;
                }

                V &operator*() {
                    assert((nullptr != hashtable) && (current_bucket < hashtable->capacity())
                           && (hashtable->buckets[current_bucket].end() != inside_bucket));
                    return *inside_bucket;
                }

                V *operator->() const {
                    assert((nullptr != hashtable) && (current_bucket < hashtable->capacity())
                           && (hashtable->buckets[current_bucket].end() != inside_bucket));
                    return &*inside_bucket;
                }

                V *operator->() {
                    assert((nullptr != hashtable) && (current_bucket < hashtable->capacity())
                           && (hashtable->buckets[current_bucket].end() != inside_bucket));
                    return &*inside_bucket;
                }

                raw_iterator &operator++() noexcept {
                    assert((nullptr != hashtable) && (current_bucket < hashtable->capacity()));
                    ++inside_bucket;
                    if (hashtable->buckets[current_bucket].end() == inside_bucket) {
                        ++current_bucket;
                        while ((current_bucket < hashtable->capacity()) && hashtable->buckets[current_bucket].empty()) {
                            ++current_bucket;
                        }
                        if (current_bucket < hashtable->capacity()) {
                            inside_bucket = hashtable->buckets[current_bucket].begin();
                        }
                        assert(!(hashtable->capacity() == current_bucket)
                               ^ (const_iterator{*this} == hashtable->cend()));
                    }
                    return *this;
                }

                raw_iterator operator++(int) const noexcept {
                    raw_iterator successor{*this};
                    return ++successor;
                }

                friend constexpr bool operator==(const raw_iterator &left, const raw_iterator &right) noexcept {
                    return (left.hashtable == right.hashtable) && (left.current_bucket == right.current_bucket)
                           && ((left.current_bucket == left.hashtable->capacity())
                               || (left.inside_bucket == right.inside_bucket));
                }

                friend constexpr bool operator!=(const raw_iterator &left, const raw_iterator &right) noexcept {
                    return !(left == right);
                }

                friend void swap(raw_iterator &left, raw_iterator &right) noexcept {
                    using std::swap;
                    swap(left.hashtable, right.hashtable);
                    swap(left.current_bucket, right.current_bucket);
                    swap(left.inside_bucket, right.inside_bucket);
                }

                friend class HashTable;

            private:
                HashTable *hashtable;
                size_type current_bucket;
                bucket_iterator inside_bucket;
        };

        pair<bucket_type &, bucket_iterator> find_unsafe(const K &key) const noexcept {
            bucket_type &bucket = buckets[hash(key)];
            for (bucket_iterator current{bucket.begin()}; bucket.end() != current; ++current) {
                if (key == current->first) {
                    return pair<bucket_type &, bucket_iterator>{bucket, current};
                }
            }
            return pair<bucket_type &, bucket_iterator>{bucket, bucket.end()};
        }

        void resize(const size_type new_capacity) {
            HashTable replacement{new_capacity};
            swap(*this, replacement);
            merge(replacement);
        }

        void rehash(const bool upwards) {
            if (upwards && (size() >= capacity())) {
                assert(size() == capacity());
                resize(capacity() * RESIZE_FACTOR);
                assert(size() * RESIZE_FACTOR == capacity());
            } else if ((!upwards) && (size() < capacity() / SHRINK_BOUNDARY_FACTOR)
                       && (capacity() >= DEFAULT_CAPACITY * RESIZE_FACTOR)) {
                // round upwards
                assert(size() + 1 == capacity() / SHRINK_BOUNDARY_FACTOR);
                // make sure we don't shrink below DEFAULT_CAPACITY
                resize(capacity() / RESIZE_FACTOR);
                assert((1 + size()) * RESIZE_FACTOR == capacity());
            }
            assert(capacity() >= DEFAULT_CAPACITY);
        }

        constexpr size_type hash(const K key) const noexcept {
            return static_cast<size_type>(HashFunction(capacity())(key));
        }

        std::unique_ptr<bucket_type[]> buckets{};
        size_type _capacity{0};
        size_type _size{0};

        static constexpr const size_type DEFAULT_CAPACITY{DEFAULT_CAPACITY_};
        static constexpr const size_type RESIZE_FACTOR{2};
        static constexpr const size_type SHRINK_BOUNDARY_FACTOR{4};
};

}  // namespace ds2

#endif  // WET_2_HASH_TABLE_HPP
