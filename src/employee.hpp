#ifndef WET_2_EMPLOYEE_HPP
#define WET_2_EMPLOYEE_HPP

#include "avl_tree.hpp"
#include "hash_table.hpp"
#include "library2.h"
#include "linked_list.hpp"
#include "pair.hpp"

#include <memory>

namespace ds2 {

struct Company;

struct Employee {
        constexpr Employee(const int id_, const int grade_, const int company_id_) noexcept
            : id{id_}
            , salary{0}
            , grade{grade_}
            , company_id{company_id_} {
        }

        int id{};
        int salary{};
        int grade{};
        int company_id{};
};

class Employees {
    private:
        class IdHash;
        using key_type = int;
        using sum_type = unsigned long;

    public:
        using mapped_type = std::shared_ptr<Employee>;

    private:
        struct ExtractGrade {
            public:
                using Abelian = sum_type;

                inline Abelian operator()(const mapped_type &employee) const noexcept {
                    return static_cast<Abelian>(employee->grade);
                }
        };

    public:
        using ById = HashTable<key_type, mapped_type, IdHash>;
        using BySalary = AVLTree<pair<int, int>, mapped_type, ExtractGrade>;
        using value_type = BySalary::value_type;

        Employees() noexcept = default;
        ~Employees() noexcept = default;

        Employees(Employees &&other) noexcept = default;
        Employees &operator=(Employees &&other) noexcept = default;

        Employees(const Employees &other) = delete;
        Employees &operator=(const Employees &other) = delete;

        bool empty() const noexcept;
        std::size_t size() const noexcept;
        bool add_employee(const mapped_type &to_add);
        mapped_type remove_employee(int employee_id) noexcept;
        mapped_type find_by_id(int employee_id) noexcept;
        bool exists(const int employee_id) const noexcept;

        template<typename ModifyEmployee>
        void bracket(const int employee_id, Employees &company, ModifyEmployee f) {
            mapped_type employee{find_by_id(employee_id)};
            const BySalary::key_type salary_key{employee->salary, employee->id};

            if (0 < employee->salary) {
#ifdef NDEBUG
                company.by_salary.erase(salary_key);
                by_salary.erase(salary_key);
#else
                assert(company.by_salary.erase(salary_key));
                assert(by_salary.erase(salary_key));
#endif
            } else {
                total_grades_without_salary -= employee->grade;
                company.total_grades_without_salary -= employee->grade;
            }

            f(*employee);

            if (0 < employee->salary) {
                const BySalary::key_type new_salary_key{employee->salary, employee->id};
#ifdef NDEBUG
                company.by_salary.emplace(new_salary_key, employee);
                by_salary.emplace(new_salary_key, employee);
#else
                assert(company.by_salary.emplace(new_salary_key, employee).second);
                assert(by_salary.emplace(new_salary_key, employee).second);
#endif
            } else {
                total_grades_without_salary += employee->grade;
                company.total_grades_without_salary += employee->grade;
            }
        }

        /// O(n_acquirer + n_target)
        Employees &merge_from_and_empty(Employees &other);

        /// O(log(n))
        StatusType sum_of_bump_grade_between_top_workers(const int m, BySalary::sum_type *result) const noexcept;

        /// O(log(n))
        StatusType average_bump_grade_between_salaries(const int low_salary,
                                                       const int high_salary,
                                                       double *result = nullptr) const noexcept;

        // TODO: Company::acquire needs (merge in O(n)) on practice 4 slide 13
        BySalary::const_iterator begin() const;
        BySalary::const_iterator end() const;

        friend void swap(Employees &left, Employees &right) noexcept;

    private:
        class IdHash {
            public:
                constexpr IdHash(const unsigned long int size_) noexcept
                    : size{size_} {
                }

                inline key_type operator()(const key_type &id) const noexcept {
                    return id % static_cast<key_type>(size);
                }

            private:
                unsigned long int size{};
        };

        ById by_id{};
        BySalary by_salary{};
        long total_grades_without_salary{0};
};

struct Company {
        Company() = default;
        ~Company() = default;
        Company(const Company &other) = delete;
        Company(Company &&other) noexcept = delete;
        Company &operator=(const Company &other) = delete;
        Company &operator=(Company &&other) noexcept = delete;

        int id{};
        int parent_id{};
        Employees employees{};
};

}  // namespace ds2

#endif  // WET_2_EMPLOYEE_HPP
