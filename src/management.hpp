#ifndef WET_2_MANAGEMENT_HPP
#define WET_2_MANAGEMENT_HPP

#include "employee.hpp"
#include "library2.h"
#include "union_find.hpp"

namespace ds2 {

class Management {
    private:
        template<typename T>
        struct ExtractCompanyValue;
        using Acquisitions = UnionFind<ExtractCompanyValue>;

    public:
        Management() = delete;

        // O(k) where k = number of companies
        explicit Management(const int companies);
        Management(const Management &other) = delete;
        Management(Management &&other) noexcept;

        Management &operator=(const Management &other) = delete;
        Management &operator=(Management &&other) = delete;

        // O(n + k) where n = number of employees
        ~Management() = default;

        // O(log*(k))
        StatusType add_employee(const int employee, const int company, const int grade);

        // O(log(n))
        StatusType remove_employee(const int employee_id);

        // O(log*(k) + n_acquirer + n_target)
        StatusType acquire_company(const int acquirer_id, const int target_id, const double factor);

        // O(log(n))
        StatusType increase_employee_salary(const int employee_id, const int salary_increase) noexcept;

        // O(log(n))
        StatusType promote_employee(int employee_id, int bump_grade) noexcept;

        // O(log*(k) + log(n))
        StatusType sum_of_bump_grade_between_top_workers_by_company(const int company_id,
                                                                    const int m,
                                                                    unsigned long *result = nullptr) const noexcept;

        // O(log*(k) + log(n))
        StatusType average_bump_grade_between_salaries_by_company(const int company_id,
                                                                  const int low_salary,
                                                                  const int high_salary,
                                                                  double *result = nullptr) const noexcept;

        // O(log*(k))
        StatusType company_value(const int company_id, double *result = nullptr) const noexcept;

        // BONUS: O(k * log(n))
        StatusType
            bump_grade_to_employees(const int lower_salary, const int higher_salary, const int bump_grade) noexcept;

        friend void swap(Management &left, Management &right) noexcept;

    private:
        inline bool valid_company_id(const int id) const noexcept {
            return (0 < id) && (acquisitions.capacity() >= static_cast<unsigned long int>(id));
        }

        inline const Company &original_company_by_id(const std::size_t company_id) const noexcept {
            return companies[company_id - 1];
        }

        inline Company &original_company_by_id(const std::size_t company_id) noexcept {
            return companies[company_id - 1];
        }

        inline const Company &company_by_id(const int company_id) const noexcept {
            return original_company_by_id(acquisitions.find(static_cast<unsigned long int>(company_id)).second);
        }

        inline Company &company_by_id(const int company_id) noexcept {
            return original_company_by_id(acquisitions.find(static_cast<unsigned long int>(company_id)).second);
        }

        inline Company &corporate_root(const int company_id) noexcept {
            Company &company = original_company_by_id(static_cast<std::size_t>(company_id));
            // TODO: refactor into a loop
            return company.id == company.parent_id ? company : corporate_root(company.parent_id);
        }

        template<typename T>
        struct ExtractCompanyValue {
            public:
                using union_find_value_type = T;
                using Result = double;

                static void unite_merge(Result &to, const Result &from, Result factor) noexcept {
                    to += factor * from;
                }

                static void find_merge(Result &to, const Result &from) noexcept {
                    to += from;
                }

                static void find_unmerge(Result &to, const Result &from) noexcept {
                    find_merge(to, -from);
                }

                static Result empty() {
                    return Result{0};
                }

                static Result initialize(const union_find_value_type &input) {
                    return static_cast<Result>(input);
                }
        };

        Acquisitions acquisitions;
        std::unique_ptr<Company[]> companies{};
        Employees employees{};
};

}  // namespace ds2

#endif  // WET_2_MANAGEMENT_HPP
