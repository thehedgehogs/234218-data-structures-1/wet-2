#include "management.hpp"

#include <cassert>
#include <cstdio>

namespace ds2 {

// O(k) where k = number of companies
Management::Management(const int companies_)
    : acquisitions{static_cast<unsigned long int>(companies_)}
    , companies{new Company[companies_]}
    , employees{} {

    assert(0 < companies_);
    for (int i{1}; i <= companies_; ++i) {
        const std::size_t index{static_cast<std::size_t>(i) - 1};
        companies[index].id = i;
        companies[index].parent_id = i;
    }
}

Management::Management(Management &&other) noexcept
    : acquisitions{0}
    , employees{} {

    swap(*this, other);
}

// O(log*(k))
StatusType Management::add_employee(const int employee_id, const int company_id, const int grade) {
    if ((0 >= employee_id) || !valid_company_id(company_id) || (0 > grade)) {
        return INVALID_INPUT;
    }

    if (employees.exists(employee_id)) {
        return FAILURE;
    }

    Employees::mapped_type employee{std::make_shared<Employee>(employee_id, grade, company_id)};
    employees.add_employee(employee);
    company_by_id(company_id).employees.add_employee(employee);

    return SUCCESS;
}

// O(log(n))
StatusType Management::remove_employee(const int employee_id) {
    if (0 >= employee_id) {
        return INVALID_INPUT;
    }

    // O(1)
    if (!employees.exists(employee_id)) {
        return FAILURE;
    }

    // O(1)
    Employees::mapped_type employee{employees.find_by_id(employee_id)};
#ifdef NDEBUG
    employees.remove_employee(employee_id);
    company_by_id(employee->company_id).employees.remove_employee(employee_id);
#else
    assert(employee);
    assert(employees.remove_employee(employee_id));
    assert(company_by_id(employee->company_id).employees.remove_employee(employee_id));
#endif

    return SUCCESS;
}

// O(log*(k) + n_acquirer + n_target)
StatusType Management::acquire_company(const int acquirer_id, const int target_id, double factor) {
    if (!valid_company_id(acquirer_id) || !valid_company_id(target_id)
        || (corporate_root(acquirer_id).id == corporate_root(target_id).id) || (0 >= factor)) {

        return INVALID_INPUT;
    }

    Company &some_acquirer = company_by_id(acquirer_id);
    Company &target = company_by_id(target_id);

    // O(n_target)
    for (const Employees::value_type &pair : target.employees) {
        pair.second->company_id = some_acquirer.id;
    }

    // O(n_acquirer + n_target)
    // TODO: make sure these moves makes sense with Roy
    employees.merge_from_and_empty(target.employees);

    acquisitions.unite(
        static_cast<unsigned long>(some_acquirer.id), static_cast<unsigned long>(target.parent_id), factor);
    target.parent_id = some_acquirer.parent_id;

    return SUCCESS;
}

// O(log(n))
StatusType Management::increase_employee_salary(const int employee_id, const int salary_increase) noexcept {
    if ((0 >= employee_id) || (0 >= salary_increase)) {
        return INVALID_INPUT;
    }

    if (!employees.exists(employee_id)) {
        return FAILURE;
    }

    Company &company = company_by_id(employees.find_by_id(employee_id)->company_id);

    employees.bracket(
        employee_id, company.employees, [salary_increase](Employee &employee) { employee.salary += salary_increase; });

    return SUCCESS;
}

// O(log(n))
StatusType Management::promote_employee(int employee_id, int bump_grade) noexcept {
    if (0 >= employee_id) {
        return INVALID_INPUT;
    }

    if (!employees.exists(employee_id)) {
        return FAILURE;
    }

    if (0 < bump_grade) {
        Company &company = company_by_id(employees.find_by_id(employee_id)->company_id);
        employees.bracket(
            employee_id, company.employees, [bump_grade](Employee &employee) { employee.grade += bump_grade; });
    }

    return SUCCESS;
}

// O(log*(k) + log(n))
StatusType Management::sum_of_bump_grade_between_top_workers_by_company(const int company_id,
                                                                        const int m,
                                                                        unsigned long *result) const noexcept {

    if (0 == company_id) {
        return employees.sum_of_bump_grade_between_top_workers(m, result);
    }

    if (!valid_company_id(company_id)) {
        return INVALID_INPUT;
    }

    const Employees &company = company_by_id(company_id).employees;
    return company.sum_of_bump_grade_between_top_workers(m, result);
}

// O(log*(k) + log(n))
StatusType Management::average_bump_grade_between_salaries_by_company(const int company_id,
                                                                      const int low_salary,
                                                                      const int high_salary,
                                                                      double *const result) const noexcept {

    if (0 == company_id) {
        return employees.average_bump_grade_between_salaries(low_salary, high_salary, result);
    }

    if (!valid_company_id(company_id)) {
        return INVALID_INPUT;
    }

    const Employees &company = company_by_id(company_id).employees;
    return company.average_bump_grade_between_salaries(low_salary, high_salary, result);
}

// O(log*(k))
StatusType Management::company_value(const int company_id, double *const result) const noexcept {
    if (!valid_company_id(company_id)) {
        return INVALID_INPUT;
    }

    const double value{acquisitions.find(static_cast<std::size_t>(company_id)).first};

    if (nullptr != result) {
        *result = value;
    }
    printf("CompanyValue: %.1lf\n", value);

    return SUCCESS;
}

// BONUS: O(k * log(n))
StatusType
    Management::bump_grade_to_employees(const int low_salary, const int high_salary, const int bump_grade) noexcept {
    if ((0 >= bump_grade) || (low_salary > high_salary)) {
        return INVALID_INPUT;
    }

    return FAILURE;
}

void swap(Management &left, Management &right) noexcept {
    using std::swap;
    swap(left.acquisitions, right.acquisitions);
    swap(left.companies, right.companies);
    swap(left.employees, right.employees);
}

}  // namespace ds2
