#ifndef WET_2_PAIR_HPP
#define WET_2_PAIR_HPP

#include <utility>

namespace ds2 {

template<typename A, typename B>
struct pair {
    public:
        friend constexpr bool operator==(const pair &left, const pair &right) noexcept {
            return (left.first == right.first) && (left.second == right.second);
        }

        friend constexpr bool operator!=(const pair &left, const pair &right) noexcept {
            return !(left == right);
        }

        friend constexpr bool operator<(const pair &left, const pair &right) noexcept {
            return (left.first < right.first) || ((left.first == right.first) && (left.second < right.second));
        }

        A first;
        B second;
};

}  // namespace ds2

#endif  // WET_2_PAIR_HPP
