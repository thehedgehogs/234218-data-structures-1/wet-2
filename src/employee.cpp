#include "employee.hpp"

#include <cassert>
#include <cstdio>
#include <limits>

namespace ds2 {

/// O(1)
bool Employees::empty() const noexcept {
    assert(by_id.empty() == by_salary.empty());
    return by_id.empty();
}

/// O(1)
std::size_t Employees::size() const noexcept {
    assert(by_id.size() == by_salary.size());
    return by_id.size();
}

/// log(n) + log(k)
bool Employees::add_employee(const Employees::mapped_type &to_add) {
    // O(1) amortized
    if (!by_id.emplace(to_add->id, to_add).second) {
        return false;
    }

    // log(k)
    if (0 < to_add->salary) {
        pair<BySalary::iterator, bool> salary_insertion =
            by_salary.emplace(BySalary::key_type{to_add->salary, to_add->id}, to_add);

        if (!salary_insertion.second) {
#ifdef NDEBUG
            by_id.erase(to_add->id);
#else
            assert(by_id.erase(to_add->id));
#endif
            return false;
        }
    } else {
        total_grades_without_salary += to_add->grade;
    }

    return true;
}

/// O(log(n))
Employees::mapped_type Employees::remove_employee(const int employee_id) noexcept {
    // log(n)
    if (by_id.end() == by_id.find(employee_id)) {
        return nullptr;
    }

    // TODO: handle no such employee id
    Employees::mapped_type employee = by_id[employee_id];

// O(1) amortized
#ifdef NDEBUG
    by_id.erase(employee_id);
#else
    assert(by_id.erase(employee_id));
#endif
    if (0 < employee->salary) {
        // log(n_company) <= log(n)
#ifdef NDEBUG
        by_salary.erase(BySalary::key_type{employee->salary, employee->id});
#else
        assert(by_salary.erase(BySalary::key_type{employee->salary, employee->id}));
#endif
    } else {
        total_grades_without_salary -= employee->grade;
    }

    return employee;
}

/// O(1) amortized
Employees::mapped_type Employees::find_by_id(const int employee_id) noexcept {
    // TODO: handle no such employee id
    assert(exists(employee_id));
    const ById::const_iterator iterator{by_id.find(employee_id)};
    if (by_id.end() == by_id.find(employee_id)) {
        return nullptr;
    }

    // TODO: handle no such employee id
    return by_id[employee_id];
}

/// O(1) amortized
bool Employees::exists(const int employee_id) const noexcept {
    return by_id.cend() != by_id.find(employee_id);
}

/// O(n_acquirer + n_target)
Employees &Employees::merge_from_and_empty(Employees &other) {
    total_grades_without_salary += other.total_grades_without_salary;
    Employees tmp{};
    // clean other completely
    swap(tmp, other);
    by_id.merge(tmp.by_id);
    by_salary.merge(std::move(tmp.by_salary));
    return *this;
}

/// O(log(n))
StatusType Employees::sum_of_bump_grade_between_top_workers(const int m,
                                                            BySalary::sum_type *const result) const noexcept {
    if (0 >= m) {
        return INVALID_INPUT;
    }

    if (by_salary.size() < static_cast<BySalary::size_type>(m)) {
        return FAILURE;
    }

    const BySalary::size_type target_rank{by_salary.size() - static_cast<BySalary::size_type>(m)};
    const BySalary::size_type to_subtract{0 < target_rank ? by_salary.sum(by_salary.find_by_rank(target_rank)->first)
                                                          : 0};
    const BySalary::sum_type sum{by_salary.sum() - to_subtract};

    if (nullptr != result) {
        *result = sum;
    }

    printf("SumOfBumpGradeBetweenTopWorkersByGroup: %lu\n", sum);
    return SUCCESS;
}

// O(log(n))
StatusType Employees::average_bump_grade_between_salaries(const int low_salary,
                                                          const int high_salary,
                                                          double *const result) const noexcept {

    if ((0 > low_salary) || (0 > high_salary) || (low_salary > high_salary)) {
        return INVALID_INPUT;
    }

    const pair<std::size_t, std::size_t> employees_without_salary = [this, low_salary, high_salary]() {
        if (0 >= low_salary) {
            assert(0 <= high_salary);
            const std::size_t count{by_id.size() - by_salary.size()};
            return pair<std::size_t, std::size_t>{static_cast<std::size_t>(count),
                                                  static_cast<std::size_t>(total_grades_without_salary)};
        }
        return pair<std::size_t, std::size_t>{0, 0};
    }();

    // find the last item before range
    const pair<int, int> from{low_salary, 0};
    const BySalary::const_iterator start{by_salary.lower_bound_reversed(from)};

    const std::size_t from_rank{by_salary.end() == start ? 0 : by_salary.rank(start->first)};
    const pair<int, int> to{high_salary, std::numeric_limits<int>::max()};
    BySalary::const_iterator finish{by_salary.upper_bound_reversed(to)};
    const std::size_t to_rank{by_salary.end() == finish ? by_salary.size() : by_salary.rank(finish->first)};
    const std::size_t count{to_rank - from_rank + employees_without_salary.first};

    if (0 < count) {
        const BySalary::sum_type from_sum{by_salary.end() == start ? 0 : by_salary.sum(start->first)};
        const BySalary::sum_type to_sum{by_salary.end() == finish ? by_salary.sum() : by_salary.sum(finish->first)};
        const double sum{static_cast<double>(to_sum - from_sum + employees_without_salary.second)};
        const double average{sum / static_cast<double>(count)};

        if (nullptr != result) {
            *result = average;
        }

        printf("AverageBumpGradeBetweenSalaryByGroup: %.1lf\n", average);
        return SUCCESS;
    }

    return FAILURE;
}

Employees::BySalary::const_iterator Employees::begin() const {
    return by_salary.begin();
}

Employees::BySalary::const_iterator Employees::end() const {
    return by_salary.end();
}

void swap(Employees &left, Employees &right) noexcept {
    using std::swap;
    swap(left.by_id, right.by_id);
    swap(left.by_salary, right.by_salary);
    swap(left.total_grades_without_salary, right.total_grades_without_salary);
}

}  // namespace ds2
