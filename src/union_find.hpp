#ifndef WET_2_UNION_FIND_HPP
#define WET_2_UNION_FIND_HPP

#include "pair.hpp"

#include <cassert>
#include <memory>

namespace ds2 {

template<template<typename> class Extract>
class UnionFind {
    public:
        using value_type = unsigned long int;
        using size_type = std::size_t;
        using ExtractValue = Extract<value_type>;
        using Result = typename ExtractValue::Result;

        UnionFind() = delete;

        explicit UnionFind(const value_type count)
            : sets{new Set[1 + count]}
            , _capacity{count}
            , _size{count} {

            // assert no overflow
            assert(0 < 1 + capacity());
            for (value_type i{0}; i <= capacity(); ++i) {
                sets[i].parent = ROOT_MARKER;
                sets[i].size = 1;
                sets[i].result = ExtractValue::initialize(i);
            }
        }

        ~UnionFind() = default;

        UnionFind(const UnionFind &other) = delete;

        UnionFind(UnionFind &&other) noexcept
            : sets{nullptr}
            , _capacity{0}
            , _size{0} {

            swap(*this, other);
        }

        UnionFind &operator=(const UnionFind &other) = delete;
        UnionFind &operator=(UnionFind &&other) = delete;

        size_type set_size(const value_type &key) noexcept {
            return sets[key].size;
        }

        template<typename... Args>
        value_type unite(const value_type &acquirer, const value_type &target, Args &&...args) noexcept {
            const value_type root1{find(acquirer).second};
            const value_type root2{find(target).second};

            if (root1 == root2) {
                return root1;
            }

            ExtractValue::unite_merge(sets[root1].result, find(target).first, std::forward<Args>(args)...);
            // TODO: proper union find merges the smaller subtree into the larger DONE
            if (sets[root2].size < sets[root1].size) {
                return merge(root2, root1);
            } else {
                return merge(root1, root2);
            }
        }

        pair<Result, value_type> find(const value_type &key) const noexcept {
#ifdef NDEBUG
            if ((0 >= key) || (key > capacity())) {
                return {ExtractValue::empty(), NO_SUCH_SET};
            }
#else
            assert((0 < key) && (key <= capacity()));
#endif

            value_type root{key};
            // accumulate all non-root results into accumulator
            Result accumulator{ExtractValue::empty()};
            while (ROOT_MARKER != sets[root].parent) {
                ExtractValue::find_merge(accumulator, sets[root].result);
                root = sets[root].parent;
            }

            if (key != root) {
                value_type current{key};
                while (ROOT_MARKER != sets[current].parent) {
                    value_type to_change{current};
                    current = sets[current].parent;

                    sets[to_change].parent = root;
                    // add to each accumulator on path only the results between itself and the root
                    ExtractValue::find_unmerge(accumulator, sets[to_change].result);
                    ExtractValue::find_merge(sets[to_change].result, accumulator);
                    assert(0 == sets[to_change].size);
                }
            }

            assert((ROOT_MARKER == sets[key].parent)
                   || ((ROOT_MARKER == sets[root].parent) && (root == sets[key].parent)));

            Result result{sets[root].result};
            if (root != key) {
                ExtractValue::find_merge(result, sets[key].result);
            }
            return pair<Result, value_type>{result, root};
        }

        const Result &result_of(const value_type &key) const noexcept {
            return sets[key].result;
        }

        Result &result_of(const value_type &key) noexcept {
            return sets[key].result;
        }

        inline size_type size() const noexcept {
            return _size;
        }

        inline size_type capacity() const noexcept {
            return _capacity;
        }

        friend void swap(UnionFind &left, UnionFind &right) noexcept {
            using std::swap;
            swap(left.sets, right.sets);
            swap(left._size, right._size);
            swap(left._capacity, right._capacity);
        }

    private:
        struct Set {
                value_type parent{ROOT_MARKER};
                size_type size{1};
                Result result{ExtractValue::empty()};
        };

        value_type merge(const value_type &from, const value_type &to) noexcept {
            assert(ROOT_MARKER == sets[from].parent);
            assert(ROOT_MARKER == sets[to].parent);

            if (find(from).second == find(to).second) {
                // nothing to do, already merged
                assert(false);
                return to;
            }

            sets[to].size += sets[from].size;
            sets[from].size = 0;
            ExtractValue::find_unmerge(sets[from].result, sets[to].result);
            sets[from].parent = to;
            --_size;
            return to;
        }

        std::unique_ptr<Set[]> sets{};
        size_type _capacity{};
        size_type _size{};

        static constexpr const value_type ROOT_MARKER{0};
        static constexpr const value_type NO_SUCH_SET{0};
};

}  // namespace ds2
#endif  // WET_2_UNION_FIND_HPP
