\documentclass[12pt, a4paper]{article}
\usepackage[margin=1in]{geometry}

\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{svg}
\graphicspath{ {graphics/}, {dry/graphics/}, {../dry/graphics/} }

\title{Wet 2 - dry part}
\author{ Gilad Woloch, Yehonatan Simian }
\date{\today}

\begin{document}

\maketitle

\section{General review of the data structures}
\subsection{The basic data structures we use}
For this assignment, we have decided to use 3 main basic data structures:
\texttt{AVLTree}, \texttt{UnionFind}, and \texttt{HashTable}.
The latter requires another data structure for its implementation - a \texttt{LinkedList}.
The data structures' parameterizations, constraints, and public APIs are described below:

\begin{description}
    \item \texttt{AVLTree} (parameterized on \texttt{K, T, ExtractAbelian, Abelian})
    \begin{itemize}
        \item A pointer to the root node.
        \item A nodes counter.
        \item An Abelian (aka \texttt{size\_type}).
    \end{itemize}
    The tree provides public API for insertion, removal and retrieval (by key) of
    stored elements. The tree also provides general utility operations:
    traversal (iterators), size and emptiness check.
    The tree is also a \textbf{rank tree}, and stores additional information in order to 
    handle the methods \texttt{sumOfBumpGrade} and \texttt{averageBumpGrade}.
    The additional information is is stored in the parameterized \texttt{Abelian},
    and it's extracted from the \texttt{Abelian} using the parameterized \texttt{ExtractAbelian} functor.

    \item \texttt{AVLTree}'s \texttt{node}
    \begin{itemize}
        \item The key-value pair.
        \item A pointer to its parent.
        \item A pointer to its left child.
        \item A pointer to its right child.
        \item The height of the sub-tree this node is the root of.
        \item The number of nodes in the sub-tree.
        \item The sum of its sub-tree's additional information (size\_type).
        \item An adjustment sum of its sub-tree's size\_type (for the bonus method).
    \end{itemize}
    
    \emph{About ownership}:
    The tree owns all nodes, either directly (only the root) or indirectly
    (transitively through other nodes). This means allocations only occur on insertion and
    de-allocations only occur on removal. The node is entirely oblivious to manual memory
    management (which keeps it simple).
    
    \item \texttt{UnionFind} (parameterized on \texttt{Extract})
    \begin{itemize}
        \item An array of \texttt{Set}s.
        \item Size.
        \item Capacity.
    \end{itemize}
    The UnionFind is implemented using upside-down trees with union by size and
    path shrinkage as taught in the lecture, in order to maintain time complexity 
    \(O(log^*(k))\) of \texttt{Union} and \texttt{Find} amortized.
    The \texttt{Extract} parameter is storing and extracting additional information in the structure.
    
    \item \texttt{UnionFind}'s \texttt{Set}
    \begin{itemize}
        \item The size (number of elements) in the set.
        \item A value of its parent.
        \item A result type of the \texttt{Extract} parameterized type.
    \end{itemize}
    Note that \texttt{Extract} \textbf{itself} must be templated.
    
    \item \texttt{HashTable} (parameterized on \texttt{K, T} and \texttt{HashFunction})
    \begin{itemize}
        \item An array of \texttt{LinkedList}s of \texttt{pair<K,T>}.
        \item Size.
        \item Capacity.
    \end{itemize}
    The hash-table is implemented with chain-hashing in order to maintain load 
    factor \(O(1)\). The parameter \texttt{K} is the key used for hashing,
    and the parameter \texttt{T} is the value stored in the table.
    The array is handled as a dynamic array, which can expand and shrink in order to maintain an
    amortized time complexity \(O(1)\), and that's why we also store it's size and capacity.
    
    \item \texttt{LinkedList} (parameterized on \texttt{T})
    \begin{itemize}
        \item A pointer to the list's front node.
        \item A pointer to the list's back node.
        \item The list's size.
    \end{itemize}
    This is a standard linked list, offering iterators for traversal on the list.
\end{description}

\newpage

\subsection{The overall data structure's design}
The requested data structure in the assignment is called \texttt{Management}.
Its sub-structures and all features are described below:
\begin{description}
    \item \texttt{Management}
        \begin{itemize}
            \item A UnionFind of \texttt{Acquisition}s (unions of companies).
            \item An array of \texttt{Company}s.
            \item An \texttt{Employees}.
        \end{itemize}

    \item \texttt{Company}
        \begin{itemize}
            \item An ID of the company (integer).
            \item An ID of its parent company (also integer).
            \item An \texttt{Employees}.
        \end{itemize}

    \item \texttt{Employees}
        \begin{itemize}
            \item A \texttt{DynamicHashTable} of \texttt{Employee}s hashed by \texttt{ID}.
            \item An \texttt{AVLTree} of pointers to \texttt{Employee}s with \(Salary>0\), 
            sorted by \texttt{pair: (Salary, ID)} 
            \item The sum of grades of employees with \(Salary==0\).
        \end{itemize}
        The last attribute is stored for the implementation for the method \texttt{averageBump}, because it might
        require the grades of employees with salary 0, but they are not stored in the \texttt{AVLTree}.
        
    \item \texttt{Employee}
        \begin{itemize}
            \item An ID.
            \item A salary.
            \item A grade.
            \item An ID of the \texttt{Company} they’re employed by.
        \end{itemize}
        
    \item \texttt{pair} (parameterized on \texttt{A} and \texttt{B})
        \begin{itemize}
            \item \texttt{A} First (an \texttt{A}).
            \item \texttt{B} Second (a \texttt{B}).
        \end{itemize}
\end{description}

\emph{About ownership}:
The above structures do not own any resources as described before, \emph{except} for the array of the companies
inside \texttt{Management}'s union-find, because this companies are not shared and therefore can be stored
in the array as the single source of truth without issues of symmetry or ownership.
\\
\\
This whole structure is illustrated in the following diagram:

\begin{figure}
    \centering
    \includegraphics[width=1\textwidth]{diagram}
    % \includesvg{diagram.svg}
    \caption{\texttt{Management} and pals}\label{fig:1-illustration}
\end{figure}

\newpage

\section{Methods' implementation}
In the following section, we describe the implementation and prove it satisfies
the complexity requirements.

\subsection{\texttt{Init}}
Constructs a \texttt{Management}, which initializes an empty AVL tree (\(O(1)\))
and an empty Hash-Table (\(O(1)\)).
In addition, an array of the \texttt{k Company}s is initialized
(with the \texttt{k} companies themselves), which takes \(O(k)\),
as well as the Union-Find of the \texttt{k} acquisitions.
Overall time complexity: \(O(k)\) as required.

\subsection{\texttt{AddEmployee}}
Find the \texttt{Company} to whom the employee is recruited to using the Union-Find.
That requires UnionFind's \texttt{Find} method, whose time complexity is \(O(log^*(k))\)
amortized with \texttt{Union}. Hence, \texttt{AddEmployee}'s time complexity is \(O(log^*(k))\),
amortized with every other method that call \texttt{Find} or \texttt{Union}, and those are
companyValue, AcquireCompany, RemoveEmployee, sumOfBumpGradeBetweenTopWorkersByGroup,
averageBumpGradeBetweenSalaryByGroup.
After finding the relevant \texttt{Company}'s ID, access it using the array (\(O(1)\)),
then construct and insert a new \texttt{Employee} to both Hash-Tables
(of the relevant \texttt{Company} and of \texttt{Management}). This action's time complexity is \(O(1)\) amortized
on average on the input (thanks to the Dynamic-Array and the constant load factor),
Therefore the total time complexity of \texttt{AddEmployee} is \(O(log^*(k))\), amortized
with the other methods mentioned before.

Note: the AVL trees stay untouched because the \texttt{Employee}'s salary is 0.

\subsection{\texttt{RemoveEmployee}}
Find the \texttt{Employee} using the Hash-Table, that's \(O(1)\) amortized.
Get the relevant \texttt{Company} using the \texttt{Employee}'s pointer, and remove the
\texttt{Employee} from both the \texttt{Company} \texttt{Management}'s Hash-Table of employees
(\(O(1)\) amortized with the insertion at \texttt{AddEmployee}), and also from the AVL trees (\(O(log(n))\)).
In total, the methods time complexity is \(O(log(n))\) amortized with \texttt{AddEmployee}.

\subsection{\texttt{AcquireCompany}}
Find the \texttt{Company}s in the array that \(O(1)\), then remove each employee from the
target \texttt{Company} and insert them to the acquirer \texttt{Company}.
The removal and insertion from the Hash-Tables takes \(O(1)\) amortized on average on input,
and the merging of the AVL trees takes \(O(n_{Acquirer} + n_{Target})\) (just like Wet-1).
Finally, unite the companies using \texttt{UnionFind}'s \texttt{Union} method, which takes
\(O(log^*(k))\) amortized with \texttt{AddEmployee}, therefore the total time complexity
of the function is \(O(log^*(k)+n_{AcquirerID}+n_{TargetID})\) amortized on average on input.

\subsection{\texttt{employeeSalaryIncrease}}
Find the \texttt{Employee} using the Hash-Table, that's \(O(1)\) amortized on average on input.
Update it's salary, then update the relevant AVL trees (both \texttt{Company} and \texttt{Management}'s)
because the salary have changed, that's \(O(log(n))\). Therefore, the method's
time complexity is \(O(log(n))\) amortized on average on input.

\subsection{\texttt{promoteEmployee}}
Find the \texttt{Employee} using the Hash-Table, that's \(O(1)\) amortized on average on input.
Update it's grade, then update the additional information (grades) in the relevant AVL trees, that's \(O(log(n))\).
Therefore, the method's time complexity is \(O(log(n))\) amortized on average on input.

\subsection{\texttt{sumOfBumpGradeBetweenTopWorkersByGroup}}
Find the \texttt{Company} using the Union-Find (\(O(log^*(k))\)).
In the AVL rank tree, find the target rank (the additional information)
and the rank to be subtracted from it (\(O(log(n))\)), then print and return the result.
Time complexity: \(O(log^*(k)+log(n))\) amortized with addEmployee, companyValue, acquireCompany, averageBumpGradeBetweenSalaryByGroup.

\subsection{\texttt{averageBumpGradeBetweenSalaryByGroup}}
Find the \texttt{Company} using the Union-Find (\(O(log^*(k))\)).
In the AVL rank tree, find the items before and after the desired range (\(O(log(n))\)),
then (if there is one or more nodes in that range), then calculate the average using the
stored sum (sum\_type) and the stored rank (size\_type) that we handled as the additional information of the tree.
If \(0\) is in the range, also consider the "sum of grades" attribute (stored in \texttt{Employees}) in the calculation,
because these employees are not store in the AVL tree.
Finally, print and return the result.
Time complexity: \(O(log^*(k)+log(n))\) amortized with addEmployee, companyValue, acquireCompany, sumOfBumpGradeBetweenTopWorkersByGroup.

\emph{Note: the "sum of grades" attribute mentioned above also handled in \texttt{addEmployee, removeEmployee, promoteEmployee} and \texttt{increaseEmployeeSalary}, in order to store the correct information whilst maintaining
time and space complexity \(O(1)\) for it.}

\subsection{\texttt{companyValue}}
Reminder: \texttt{companyValue} is the only method that relates to the original \texttt{Company},
and not the acquirer \texttt{Company} (like all of the other methods in \texttt{Management}).
The calculation is achieved using the "Boxes tower method" taught in exercise 4 in UnionFind's tutorial.
Time complexity: \(O(log^*(k))\) amortized with addEmployee, acquireCompany, averageBumpGradeBetweenSalaryByGroup, averageBumpGradeBetweenSalaryByGroup.

\subsection{\texttt{Quit}}
Destructs the entire \texttt{Management}, which in turn destructs the AVL tree, the Hash-Table, and the \texttt{Company}s
in the array. Each of the structures in \texttt{Management} is destructed in liner time.
There are \texttt{k} companies, with determine the sizes of the array and the Union-Find.
There are \texttt{n} employees, each appears in 4 nodes (like wet-1's nodes).
In total, at most \(2k+5n=O(k+n)\) values are destructed, QED.

\newpage

\section{Proof of space complexity requirements}
The requested space complexity for the overall data structure
(\texttt{Management}) is \(O(n+k)\) in the worst case, where \(n\) is the
number of employees, and \(k\) is the number of companies.

\subsection{Data structure space complexity proof}
First, note that all structs in use are constant, so the overall space
complexity is linear to the total size of the trees.  We need to sum the total
size of all trees. In order to achieve this, we take into account the fact
that each tree's specific size is irrelevant - we only care about the total
count. Thus, we can change the order of summation and count how many nodes
each value in the system requires. A \texttt{Company} is stored in \texttt{Management}'s array,
and for each \texttt{Company} we increase the Union-Find's size by 1.
An Employee is pointed to from 2 \texttt{Employees} in Management and in its employer Company.
Each Employees hold 2 synchronized trees, so in total every Employee is pointed to from 4 nodes.

In total, we have
\[
    n_{\text{employees}} * (2_{\text{trees}} + 2_{\text{hash tables}})
    + k_{\text{companies}} * (1_{\text{array}} + 1_{\text{union find}}) = O(n + k)
\]
QED

\subsection{Methods space complexity proof}
The space complexity for each method of \texttt{Management} isn't explicitly
defined, but it is forbidden to overtake the space complexity of the overall
data structure - therefore we shall prove that each method does not exceed
space complexity of \(O(n+k)\).

\subsubsection{\texttt{Init}} The method initializes an array and a UnionFind of size \texttt{k},
and HashTables and AVL Trees with no nodes, therefore the method's space complexity is \(O(k)\).

\subsubsection{Provided by data structures' APIs}
\begin{itemize}
    \item \texttt{addEmployee}
    \item \texttt{removeEmployee}
    \item \texttt{employeeSalaryIncrease}
    \item \texttt{promoteEmployee}
    \item \texttt{sumOfBumpGradeBetweenTopWorkersByGroup}
    \item \texttt{averageBumpGradeBetweenSalaryByGroup}
    \item \texttt{companyValue}
\end{itemize}
These methods only use the data structures' APIs: insertion, removal, traversal and retrieval;
All of them require constant space (no variable dynamic allocations etc.),
therefore their space complexity is \(O(1)\).

\subsubsection{\texttt{Quit}}
The method erases the entire trees in the data structure, which takes no
additional variables, therefore the method's space complexity is \(O(1)\), as
required.

\end{document}
