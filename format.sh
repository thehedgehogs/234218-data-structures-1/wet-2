#!/usr/bin/env bash

set -Eeuxo pipefail

if [[ $# -gt 0 ]]; then
    clang-format -i -Werror $@
else
    clang-format -i -Werror src/*.{c,h}pp {public,private}-tests/*.cc
fi
